%% oneGameFn
% v1.0 22 Jun 2018
%
% Function that takes the parameters required for a single run of the
% iterated asymmetric probabilitistic Hawk-Dove game and runs a simulation
% where Animal A and Animal B puruse specified strategies, calculating the
% total rewards to each animal.

function[...
    rewardsTotal,...        Total rewards to Animal A and Animal B
    resultsStore...         Store of various results for each animal (only produced if flagged, see note below)
    ] = oneGameFn(...
    gameParams,...          Cell array containing parameters for running the game
    stratNames,...          Cell array of names of strategies purused by Animal A and Animal B (see translateStratName for details).
    pA...                   Probability of Animal A winning in a fight with Animal B
    )

% resultsStore summary
%  * resultsStore is a 1-by-3 cell array, with the first part corresponding to
%  Animal A, the second to Animal B, and the third to general results.
%  * resultsStore{1} and resultsStore{2} contain strategies in use and
%  (where appropriate) beta parameters.
%  * resultsStore{3} contains the probability of each animal playing Dove,
%  the outcomes for Animal A, and the rewards to each animal from that
%  round.

% Check that variables in gameParams are within acceptable ranges.
checkGameParams(gameParams);

% Extract fightCosts from cell array of game parameters
fightCosts = gameParams{1};

% Initialise the strategy type, the private parameters of the strategy, and
% the private variables of the strategy.
[stratTypes,privParams,privVars] = ...
    initialiseStrats(fightCosts,stratNames);

% Update privVars (as needed) using information that depends on the
% probability of A beating B in a Hawk-Hawk fight.
privVars = updateInitialPrivVars(pA,fightCosts,stratTypes,privParams,privVars);

% Use runGame to produce the total rewards (and, if relevant, the complete
% results storage).
[rewardsTotal,resultsStore] = runGame(pA,gameParams,stratTypes,privParams,privVars,stratNames);
