%% constructProbsList.m
% v1.0 22 Jun 2018
%
% Function to construct stratPairsList, a list of all of the strategy pairs
% to be used in the tournament or related system where many games are run.

function ...
    stratPairsList ...          List of strategy pairs to be used in runManyGames
    = constructStratPairs(...
    stratDetails...             Cell array containing details of how to construct list of strategy pairs (see below).
    )

%% Extract information from stratDetails

% List of strategies to be compared in tournament
stratList = stratDetails{1};
      
% Flag to indicate whether to run simulations for all possible strategy
% pairs or whether to take advantage of symmetries. For example, should a
% game where Animal A plays Strategy X and Animal B plays Strategy Y be
% 'repeated' where Animal A plays Strategy Y and Animal B plays Strategy X
% or should these be treated as being equivalent.
allStratPairsFlag = stratDetails{2};

%% Construct list of strategy pairs

% Construct two column list of different strategy pairs. 
numStrats = numel(stratList);
if allStratPairsFlag
    
    % If all strategy pairs are to be used, the list contains numStrats^2
    % different pairs, which can easily be constructed using modular
    % arithmetic.
    numStratPairs = numStrats^2;
    stratPairsList = cell(numStratPairs,2);
    for iStratPairs = 1:numStratPairs
        stratPairsList{iStratPairs,1} = stratList{ceil(iStratPairs/numStrats)};
        stratPairsList{iStratPairs,2} = stratList{rem((iStratPairs-1),numStrats)+1};
    end
    
else
    
    % If symmetry is being exploited, the list contains the numStrats
    % triangular number of different pairs, and it is more convenient to
    % construct the pairs using loops.
    numStratPairs = (numStrats^2 + numStrats)/2;
    stratPairsList = cell(numStratPairs,2);
    iStratPairs = 0;
    for iStratA = 1:numStrats
        for iStratB = iStratA:numStrats
            iStratPairs = iStratPairs + 1;
            stratPairsList{iStratPairs,1} = stratList{iStratA};
            stratPairsList{iStratPairs,2} = stratList{iStratB};
        end
    end
end