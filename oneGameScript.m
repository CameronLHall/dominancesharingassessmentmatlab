%% oneGameScript.m
% v1.0 22 Jun 2018
%
% Script m-file to run a single game of the iterated asymmetric
% probabilistic Hawk-Dove game.
%
% Inputs are all given in the first section of the code, and include
% * cW and cL (costs to winners and losers associated with fighting)
% * numRounds (number of rounds in game)
% * discountRate (proportional decrease in value of each new round)
% * stratNameA (strategy used by Animal A)
% * stratNameB (strategy used by Animal B)
% * pA (probability that Animal A wins fights)
%
% Other input parameters can also be modified; they are all described in
% the first section of the code below.
%
% Main outputs are rewardsTotal and resultsStore.
%
% rewardsTotal: Total rewards to Animal A and Animal B (1-by-2 vector)
%
% resultsStore: Cell array containing details for both animals from every
% round (including private variables whose meaning is dependent on the
% strategy used). The details of resultsStore can be found in runGame, but
% in summary:
%  * resultsStore is a 1-by-3 cell array, with the first part corresponding to
%  Animal A, the second to Animal B, and the third to general results.
%  * resultsStore{1} and resultsStore{2} contain strategies in use and
%  (where appropriate) beta distribution parameters.
%  * resultsStore{3} contains the probability of each animal playing Dove,
%  the outcomes for Animal A, and the rewards to each animal from that
%  round.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Specify parameters and strategy names

% 1-by-2 vector of cW (cost of winning a fight) and cL (cost of losing a
% fight)
fightCosts = [0.1 0.2];

% Number of rounds in iterated game
numRounds = 1000;

% Discount rate (if discount rate < 1, future games are worth less than
% past games). Note that 0 < discountRate <= 1.
discountRate = 0.995;

% Probability that Animal A wins a fight.
pA = 0.1;

% Storage flag
storeFlag = true;

% Trembling hand probability. Probability that each animal changes its
% strategy at the last minute in each round. This should normally be small,
% and definitely be <1/2. While important for certain forms of game
% theoretical stability, the trembling hand makes less sense in a
% biological context and it should normally be set to zero.
trembling = 0;

% Names of strategies used by Animal A and Animal B
stratNameA = 'Mean pWin PDTfT with flat prior';
stratNameB = 'Grim';

% INDEX OF STRATEGY NAMES (all strategies above should be taken from this
% list)
%
% For full definitions, see translateStratName
%
% NONLEARNING STRATEGIES
% '25% Dove'
% '50% Dove'
% '75% Dove'
% 'Always Dove'
% 'Always Hawk'
% 'Bully'
% 'Grim'
% 'Informed TfT'
% 'Modified Pavlov'
% 'Mixed Nash'
% 'Nash'
% 'Pavlov'
% 'PD TfT'
% 'Selfish'
% 'Snowdrift TfT'
% 'Tit for Tat'
%
% LEARNING STRATEGIES
% 'Weighted PDTfT with flat prior'
% 'Weighted PDTfT with aggressive prior'
% 'Weighted PDTfT with very-aggressive prior'
% 'Weighted PDTfT with super-aggressive prior'
% 'Mean pWin PDTfT with flat prior'
% 'Mean pWin PDTfT with aggressive/flat priors'
% 'Mean pWin PDTfT with very-aggressive/flat priors'
% 'Mean pWin PDTfT with super-aggressive/flat priors'
% 'Median pWin PDTfT with flat prior'
% 'Median pWin PDTfT with aggressive/flat priors'
% 'Median pWin PDTfT with very-aggressive/flat priors'
% 'Median pWin PDTfT with super-aggressive/flat priors'
% '70-80 percentile pWin PDTfT with flat prior'
% '80-90 percentile pWin PDTfT with flat prior'
% '90-95 percentile pWin PDTfT with flat prior'
% '95-98 percentile pWin PDTfT with flat prior'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% PARAMETER SPECIFICATION HAPPENS ABOVE THIS LINE
%%%%% THERE IS NO NEED TO MODIFY CODE BELOW THIS LINE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Run game with given parameters

% Collect game parameters into cell arrray used by oneGameFn
gameParams = {fightCosts, numRounds, discountRate,storeFlag,trembling};

% Check that variables in gameParams are within acceptable ranges.
checkGameParams(gameParams);

% Collect strategy names into cell array used by oneGameFn
stratNames = {stratNameA, stratNameB};

% Run oneGameFn to obtain total rewards to Animal A and Animal B
[rewardsTotal,resultsStore] = oneGameFn(gameParams,stratNames,pA);
