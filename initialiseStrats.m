%% initialiseStrats.m
% v1.0 22 Jun 2018
%
% Function that initialises the strategy type, private parameters and
% private variables of Animal A and Animal B strategies as defined by the
% strings of their names (using translateStratName), and then processes the
% private parameters (where appropriate) using information from the costs
% of winning and losing.

function[...
    stratTypes,...          1-by-2 vector of integers indicating type of learning strategy
    privParams,...          1-by-2 cell array of cell arrays of parameters (specific to learning type)
    privVars...             1-by-2 cell array of cell arrays of variables (specific to learning type)
    ] = initialiseStrats(...
    fightCosts,...          1-by-2 vector of cW (cost of winning a fight) and cL (cost of losing a fight)
    stratNames...           1-by-2 cell array of strings indicating strategies (see translateStratName for details)
    )

%% Initialisations

% LIST OF STRATEGY TYPES THAT USE STAGE GAME CLASSIFICATIONS 
% This will be most learning strategies and most informed strategies - any
% strategy where an animal's behaviour depends on the classification of the
% stage game. In this case, the private parameter set needs to be modified
% using simplifySubstratList to take account of the classifications.
stratsUsingStageGameClass = [0 10 20 30 31];

% Initialise stratType, privParams, privVars, and the temporary
% privParamsFull (an output of translateStratName).
stratTypes = zeros(1,2);
privParams = cell(1,2);
privVars = cell(1,2);

%% Main function

% Use classifyStageGameSeq to determine the sequence of stage games that
% are encountered as the probability of self winning a fight varies from 0
% to 1, and the critical pWin values at which these changes happen.
[stageGameSeq,pWinThresholds] = classifyStageGameSeq(fightCosts);

% Loop through Animal A and Animal B
for iAnimal = 1:2
    
    % Use translateStratName to determine values for stratType, privParams
    % and privVars.
    [stratTypes(iAnimal),privParams{iAnimal},privVars{iAnimal}] ...
        = translateStratName(stratNames{iAnimal});
    
    % In the case of a strategy that uses the stage game classifications
    % (which will be most strategies), use simplifySubstratList to simplify
    % the first two terms in the private parameter set (which define (i)
    % the different  memory-one substrategies used, and (ii) the
    % substrategy for each stage game classification), into only the
    % relevant substrategies used, and the associated critical values of
    % pWin. If this is not relevant, pass the private parameters on
    % unchanged.
    if any(stratTypes(iAnimal) == stratsUsingStageGameClass)
        privParams{iAnimal} = ...
            simplifySubstratList(stageGameSeq,pWinThresholds,privParams{iAnimal});
    end
end

