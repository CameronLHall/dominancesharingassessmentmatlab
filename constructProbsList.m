%% constructProbsList.m
% v1.0 22 Jun 2018
%
% Function to construct allProbs, a list of all the probabilites to be used
% in a repeated set of games. This list has a random element, and is drawn
% from a probability distribution defined by probParams.

function ...
    allProbs ...                List of all probabilities of Animal A winning a fight to be used in a tournament/large set of games
    = constructProbsList(...
    probParams...               Cell array of parameters defining the probability distribution to be used to construct allProbs
    )

%% Extract variables from probParams

% Number of (independent) probabilities to be used in repeated set of
% games.
numProbs = probParams{1};

% Flag to indicate whether the method of complementary random numbers
% should be used. If set to true, allProbs will be constructed so that
% there are numProbs/2 (rounded up) independent probabilities; the other
% probabilities are obtained by taking the complement (pNew = 1 - pOld) of
% these. Using the method of complementary random numbers will reduce the
% variance in the average reward obtained and improve its accuracy.
useComplementaryRandomNumbers = probParams{2};

% The probability of Animal A winning a fight is drawn from a symmetric
% beta distribution with this parameter. trueBetaParamForWinProbs = 1
% corresponds to a uniform distribution of win probabilities.
trueBetaParamForWinProbs = probParams{3};

%% Construct allProbs

% Construct sorted list of probabilities drawn from a uniform distribution.
% In the case where the method of complementary random numbers is used,
% introduce that at this stage.
if useComplementaryRandomNumbers
    numProbsTemp = round(numProbs/2);
    allProbsTemp = rand(numProbsTemp,1);
    allProbs = sort([allProbsTemp; 1-allProbsTemp]);
else
    allProbs = sort(rand(numProbs,1));
end

% If the true distribution of win probabilities is not uniform, use the
% inverse CDF of the beta distribution to rescale these to obtain
% probabilities drawn from the appropriate beta distribution.
if trueBetaParamForWinProbs ~= 1
    allProbs = ...
        betaincinv(allProbs,trueBetaParamForWinProbs,trueBetaParamForWinProbs);
end