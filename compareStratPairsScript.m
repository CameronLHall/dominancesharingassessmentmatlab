%% compareStratPairsScript.m
% v1.0 22 Jun 2018
%
% Script m-file to compare the performance of different strategy pairs
% over a full range of pA values.
%
% Inputs are all given in the first section of the code, and include
% * cW and cL (costs to winners and losers associated with fighting)
% * numRounds (number of rounds in game)
% * discountRate (proportional decrease in value of each new round)
% * stratPairsList (list of strategy pairs to be compared)
% * numProbs (number of different pA values to use in comparing strategies)
% * numProbRepeats (number of repeats to run at each pA value)
%
% Other input parameters can also be modified; they are all described in
% the first section of the code below.
%
% Main output is avgRewardsStore:
% This is a numStratPairs-by-numProbs-by-2 array where each
% entry is the averaged overall reward to a particular animal in a
% particular game (where its opponent and probability of winning a fight
% are kept the same).
%
% This can be plotted to show how the performance of Animal A and Animal B
% vary with pA where they pursue the strategies indicated.

%% Specify parameters and strategy names

% 1-by-2 vector of cW (cost of winning a fight) and cL (cost of losing a fight)
fightCosts = [0.1 0.2];

% Number of rounds in iterated game
numRounds = 500;

% Discount rate (if discount rate < 1, future games are worth less than
% past games). Note that 0 < discountRate <= 1.
discountRate = 0.995;

% Trembling hand probability. Probability that each animal changes its
% strategy at the last minute in each round. This should normally be small,
% and definitely be <1/2. While important for certain forms of game
% theoretical stability, the trembling hand makes less sense in a
% biological context and it should normally be set to zero.
trembling = 0;

% List of strategy pairs to be compared (2 columns, numStratPairs rows)
stratPairsList = {
    'Weighted PDTfT with flat prior',...
    'Weighted PDTfT with flat prior';...
    'Weighted PDTfT with flat prior',...
    'Weighted PDTfT with aggressive prior';...
    'Weighted PDTfT with flat prior',...
    'Weighted PDTfT with very-aggressive prior'...
    };

% INDEX OF STRATEGY NAMES (all strategies above should be taken from this
% list)
%
% For full definitions, see translateStratName
%
% NONLEARNING STRATEGIES
% '25% Dove'
% '50% Dove'
% '75% Dove'
% 'Always Dove'
% 'Always Hawk'
% 'Bully'
% 'Grim'
% 'Informed TfT'
% 'Modified Pavlov'
% 'Mixed Nash'
% 'Nash'
% 'Pavlov'
% 'PD TfT'
% 'Selfish'
% 'Snowdrift TfT'
% 'Tit for Tat'
%
% LEARNING STRATEGIES
% 'Weighted PDTfT with flat prior'
% 'Weighted PDTfT with aggressive prior'
% 'Weighted PDTfT with very-aggressive prior'
% 'Weighted PDTfT with super-aggressive prior'
% 'Mean pWin PDTfT with flat prior'
% 'Mean pWin PDTfT with aggressive/flat priors'
% 'Mean pWin PDTfT with very-aggressive/flat priors'
% 'Mean pWin PDTfT with super-aggressive/flat priors'
% 'Median pWin PDTfT with flat prior'
% 'Median pWin PDTfT with aggressive/flat priors'
% 'Median pWin PDTfT with very-aggressive/flat priors'
% 'Median pWin PDTfT with super-aggressive/flat priors'
% '70-80 percentile pWin PDTfT with flat prior'
% '80-90 percentile pWin PDTfT with flat prior'
% '90-95 percentile pWin PDTfT with flat prior'
% '95-98 percentile pWin PDTfT with flat prior'

% Number of probabilities of Animal A winning to use in strategy
% comparison.
numProbs = 500;

% Flag to indicate whether the method of complementary random numbers
% should be used. If set to true, allProbs will be constructed so that
% there are numProbs/2 (rounded up) independent probabilities; the other
% probabilities are obtained by taking the complement (pNew = 1 - pOld) of
% these. Using the method of complementary random numbers will reduce the
% variance in the average reward obtained and improve its accuracy.
useComplementaryRandomNumbers = true;

% The probability of Animal A winning a fight is drawn from a symmetric
% beta distribution with this parameter, which must be greater than 0.
% trueBetaParamForWinProbs = 1 corresponds to a uniform distribution of win
% probabilities.
trueBetaParamForWinProbs = 1;

% Number of repeats at each probability
numProbRepeats = 20;

% Flag to indicate whether to display progress after each strategy pair
displayResultsForStratPairs = true;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% PARAMETER SPECIFICATION HAPPENS ABOVE THIS LINE
%%%%% THERE IS NO NEED TO MODIFY CODE BELOW THIS LINE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Collect and check parameters; construct allProbs, and run games.

% Collect parameters into cell arrays

% NOTE: storeFlag is set to false, since the information stored in a one
% game version of this code is not used in the tournament version.

gameParams = {fightCosts, numRounds, discountRate, false, trembling};
probParams = {numProbs, useComplementaryRandomNumbers, trueBetaParamForWinProbs, numProbRepeats};

% Check that variables in gameParams are within acceptable ranges.
checkGameParams(gameParams);

% Check that variables in probParams are within acceptable ranges.
checkProbParams(probParams);

% Construct vector containing list of pA values to use in the games.
allProbs = constructProbsList(probParams);
numProbs = numel(allProbs);

% Use runManyGames to run the main game repatedly using the given
% list of strategies and the given list of probabilities.
tic
rewardsStore = runManyGames(gameParams,stratPairsList,allProbs,numProbRepeats,displayResultsForStratPairs);
toc

%% Simplify storage and construct figures

% avgRewardsStore is a numStratPairs-by-numProbs-by-2 array where each
% entry is the averaged overall reward to a particular animal in a
% particular game (where its opponent and probability of winning a fight
% are kept the same).
avgRewardsStore = permute(mean(rewardsStore,3),[1 2 4 3]);

% Construct figures showing the rewards to the different strategies as a
% function of pA.
figuresForStratPairs(avgRewardsStore,stratPairsList,allProbs)
