%% checkGameParams
% v1.0 22 Jun 2018

%
% Error checking for the gameParams cell array.

function checkGameParams(gameParams)

%% Extract variables from gameParams
fightCosts = gameParams{1};
numRounds = gameParams{2};
discountRate = gameParams{3};
if numel(gameParams) > 3
    storeFlag = gameParams{4};
else
    storeFlag = false;
end
if numel(gameParams) > 4
    trembling = gameParams{5};
else
    trembling = 0;
end
    
%% Error checking
if numel(fightCosts) ~= 2
    error('fightCosts is incorrect size: must be a 1-by-2 vector containing cW and cL')
end

if fightCosts(1) < 0
    error('Problem with fightCosts: cost of winning a fight is negative.');
end

if fightCosts(2) < 0 
    error('Problem with fightCosts: cost of losing a fight is negative.');
end

if fightCosts(1) - fightCosts(2) > 1
    error('Problem with fightCosts: winning a fight is more costly than losing it.');
end

if floor(numRounds)~=numRounds || numRounds <= 0 || isinf(numRounds)
    error('Problem with numRounds: specified number of rounds is not a positive integer')
end

if discountRate > 1
    error('Problem with discountRate: specified discount rate is greater than one.')
elseif discountRate <= 0
    error('Problem with discountRate: specified discount rate is negative.')
end

if ~islogical(storeFlag)
    error('Problem with storeFlag: flag should be logical variable')
end

if trembling > 0.5
    error('Problem with trembling hand probability: specified probability is greater than 1/2.')
elseif trembling < 0
    error('Problem with trembling hand probability: specified probability is negative.')
end