%% runManyGames.m
% v1.0 22 Jun 2018
%
% Function that takes input of game parameters, a list of strategy pairs, a
% list of probabilities of Animal A winning a fight, and the number of
% times to run games at each probability, and runs the iterated asymmetric
% probabilistic Hawk-Dove game repeatedly, storing the rewards to each
% animal from each game.

function...
    rewardsStore...                 numStratPairs-by-numProbs-by-numProbRepeats-by-2 array containing total reward to each animal after each game
    = runManyGames(...
    gameParams,...                  Cell array containing parameters for running the game (costs of fighting, number of rounds, discount rate) used by runGame
    stratPairsList,...              numStratPairs-by-2 cell array of strategy pairs to be considered
    allProbs,...                    numProbs-by-1 vector containing all pA values to be used in runs
    numProbRepeats,...              Number of times each possible combination of pA and strategy pair should be repeated
    displayResultsForStratPairs...  Flag to indicate whether results should be displayed progressively in main Matlab screen to help gauge progress
    )

%% Initialisations

% Start the timer if results for strategy pairs and timings are being shown
% progressively.
if displayResultsForStratPairs
    tic
end

% Initialise cell array containing the strategy names for any particular
% run of the game.
stratNames = cell(1,2);

% Sizes of various input arrays
numStratPairs = size(stratPairsList,1);
numProbs = numel(allProbs);

% Extract fightCosts from gameParams
fightCosts = gameParams{1};

% Initialise storage for the overall results of every game.
rewardsStore = zeros(numStratPairs,numProbs,numProbRepeats,2);

%% Loop through all strategy pairs
for iStratPairs = 1:numStratPairs
    
    % Extract the appropriate strategy names to be used from
    % stratPairsList.
    stratNames{1} = stratPairsList{iStratPairs,1};
    stratNames{2} = stratPairsList{iStratPairs,2};
    
    % Initialise the strategy type, the private parameters of the strategy, and
    % the private variables of the strategy.
    [stratTypes,privParams,privVars] = ...
        initialiseStrats(fightCosts,stratNames);
    
    for iWinProbs = 1:numProbs
        
        % Probability of A beating B for this set of simulations
        pA = allProbs(iWinProbs);
        
        % Update privVars (as needed) using information that depends on the
        % probability of A beating B in a Hawk-Hawk fight.
        privVars = updateInitialPrivVars(pA,fightCosts,stratTypes,privParams,privVars);
        
        for iRepeatGames = 1:numProbRepeats
            
            % Use runGame to obtain total rewards to Animal A and Animal B
            % in this particular iteration.
            rewardsTotal = runGame(pA,gameParams,stratTypes,privParams,privVars,stratNames);
            rewardsStore(iStratPairs,iWinProbs,iRepeatGames,:) = rewardsTotal;
            
        end
        
    end
    
    if displayResultsForStratPairs
        
        averageRewards = mean(mean(rewardsStore(iStratPairs,:,:,:),3),2);
        
        disp(' ');
        disp(['Strategy pair ' num2str(iStratPairs) ' of ' num2str(numStratPairs)]);
        disp(['Animal A strategy: ''' stratNames{1} '''']);
        disp(['Animal B strategy: ''' stratNames{2} '''']);
        disp(['Average rewards to Animal A: ', num2str(averageRewards(1))]);
        disp(['Average rewards to Animal B: ', num2str(averageRewards(2))]);
        toc
        
        disp(' ');
        
    end
    
    
end    