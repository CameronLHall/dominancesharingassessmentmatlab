%% updateInitialPrivVars.m
% v1.0 Jun 2018

%
% Function that does an initial update of privVars using information
% obtained from pA. The initialiseStrats routine, which creates privVars,
% does not use any information from pA so as to streamline the process of
% running many game simulations with different pA values. However, informed
% strategies (which start with complete knowledge of pA), or partially
% informed strategies (where the animals begin with an estimate of pA)
% require information about pA to be included in their private variables.
% This is dealt with using this function.
% NOTE: One run of this function is sufficient to deal with the private
% variables of both animals.
% NOTE ALSO: No "partially informed" strategies have yet been defined but
% this is where they would be implemented.

function [...
    privVars...                     1-by-2 cell array of cell arrays of variables (specific to learning type)
    ] = updateInitialPrivVars(...
    pA,...                          Probability of Animal A winnning a fight
    fightCosts,...                  Vector of cW (cost of winning a fight) and cL (cost of losing a fight)
    stratType,...                   1-by-2 vector of integers indicating type of learning strategy
    privParams,...                  1-by-2 cell array of cell arrays of parameters (specific to learning type)
    privVars...                     1-by-2 cell array of cell arrays of variables (specific to learning type)
    )

%% Forms of pA used by informed strategies

% Probability of winning a fight for Animal A and Animal B
winProbs = [pA 1-pA];

% Expected reward from a fight for Animal A and Animal B
muAB = [ -fightCosts(2) + pA*(1-fightCosts(1)+fightCosts(2)), ...
        1-fightCosts(1) - pA*(1-fightCosts(1)+fightCosts(2))];

% 2-by-2 array where the ith row is the expected reward for self and
% opponent.
muArray = [muAB; [muAB(2) muAB(1)]];
        

% Loop through Animal A and Animal B
for iAnimal = 1:2

    % Method used for updating privVars will depend on strategy type (and
    % often won't be used).
    switch stratType(iAnimal)
        
        case 0
            % For an informed strategy (i.e. where the memory-one strategy
            % to be used depends only on the stage game classification),
            % the only needed information is the probability of winning.
            privVars{iAnimal}{2} = winProbs(iAnimal);
            
        case 1
            % For a generalised memory-one strategy, the full details of
            % the expected rewards for both animals may be needed.
            privVars{iAnimal}{2} = muArray(iAnimal,:);
            
        otherwise
            % NOTE: No error for otherwise in this case, as update of
            % initial private variables may not be needed.
            
    end

end
