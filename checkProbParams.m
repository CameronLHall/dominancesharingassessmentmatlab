%% checkProbParams.m
% v1.0 22 Jun 2018
%
% Error checking for the probParams cell array.

function checkProbParams(probParams)

%% Extract variables from probParams
numProbs = probParams{1};
symmetricWinProbsFlag = probParams{2};
trueBetaParamForWinProbs = probParams{3};
numProbRepeats = probParams{4};

%% Error checking

if numProbs < 1 || ceil(numProbs) ~= numProbs
    error('Problem with numProbs: number of probabilities must be a positive integer');
end

if numProbRepeats < 1 || ceil(numProbs) ~= numProbs
    error('Problem with numProbRepeats: number of iterations must be a positive integer');
end

if ~islogical(symmetricWinProbsFlag)
    error('Problem with symmetricWinProbsFlag: must be a logical variable');
end

% Check if parameter for beta distribution of win probabilities is
% appropriate (must be >0).
if trueBetaParamForWinProbs <= 0
    error('Error with beta parameter for generating win probabilities: input parameter is less than 0');
end

