%% figuresForTournament.m
% v1.0 22 Jun 2018
%
% Function to generate a figure for each strategy in the list, showing the
% averaged rewards to an animal pursuing that strategy as a function of its
% probability of winning. Multiple lines are shown on the one plot, showing
% the reward to the animal pursuing the specified strategy against a range
% of opponents.

function ...
    figuresForTournament(...
    avgRewardsStore,...         numStratPairs-by-numProbs-by-2 array of the average rewards associated with each strategy pair (dim 1) for each probability of Animal A winning (dim 2) for Animal A and Animal B (dim 3).
    stratDetails,...            1-by-2 cell array containing list of strategy names (cell 1) and flag to indicate whether or not the tournament run considered every possible strategy pair or whether symmetry was exploited (cell 2).
    allProbs...                 numProbs-by-1 vector of all pA values used in simulations
    )

% Extract information from stratDetails
stratList = stratDetails{1};
allStratPairsFlag = stratDetails{2};
numStrats = numel(stratList);

% Vectors of win probabilities for Animal A and Animal B to be used in
% plotting. (These will be identical if symmetricWinProbsFlag is true.)
probWinA = allProbs;
probWinB = 1-allProbs;

% Colour ordering to be used in figures 
colOrd = lines(numStrats);

% Initialisation for the plots to be used in the legend.
% NOTE: Dashed lines will be used to indicate results from Animal B and
% complete lines indicate results from Animal A, but all are plotted
% together.
plotsForLegend = gobjects(numStrats,1);

% Loop through all strategies, creating a figure for each strategy's
% rewards to self.
for iSelfStrat=1:numStrats

    figure
    hold on
    
    % Loop through all possible opponent strategies, adding a line (or two)
    % to the plot to indicate the rewards to the 'self strategy' against
    % this opponent.
    for iOppStrat = 1:numStrats
    
        if allStratPairsFlag
            
            % In the case where all strategy pairs are used (not exploiting
            % symmetry), each opposition strategy will give rise to two
            % lines: one where the self strategy is used by Animal A (shown
            % as a continuous line) and another where the self strategy is
            % used by Animal B (shown as a dashed line). Algebra for
            % working out which line in avgRewardsStorage this corresponds
            % to is relatively simple.
            
            plotsForLegend(iOppStrat) = ...
                plot(probWinA,avgRewardsStore((iSelfStrat-1)*numStrats+iOppStrat,:,1),'Color',colOrd(iOppStrat,:));
                plot(probWinB,avgRewardsStore((iOppStrat-1)*numStrats+iSelfStrat,:,2),'Color',colOrd(iOppStrat,:),'LineStyle','--');
    
        else
 
            % In the case where we exploit symmetry to reduce the number of
            % game simulations run, each opposition strategy will give rise
            % to one line except in the special case where the strategy
            % competes against itself. Algebra for working out which line
            % in avgRewardsStorage this corresponds to is messier, but can
            % be done by considering triangle numbers, as indicated in the
            % formulas for relevantLineForPlot.
            
            if iOppStrat <= iSelfStrat
            
                relevantLineForPlot = ...
                    numStrats*(numStrats+1)/2 ...
                    - (numStrats-iOppStrat+1)*(numStrats-iOppStrat+2)/2 ...
                    + iSelfStrat-iOppStrat+1;

                plotsForLegend(iOppStrat) = plot(probWinB,avgRewardsStore(relevantLineForPlot,:,2),'Color',colOrd(iOppStrat,:),'LineStyle','--');
                
            end
            
            if iOppStrat >= iSelfStrat
                
                relevantLineForPlot = ...
                    numStrats*(numStrats+1)/2 ...
                    - (numStrats-iSelfStrat+1)*(numStrats-iSelfStrat+2)/2 ...
                    + iOppStrat-iSelfStrat+1;
                
                plotsForLegend(iOppStrat) = plot(probWinA,avgRewardsStore(relevantLineForPlot,:,1),'Color',colOrd(iOppStrat,:));
                
            end
                
                                
        end
        
    end
    
    % Title and legend for plot
    title(['Rewards for ''' stratList{iSelfStrat} ''' against various opponents']);
    xlabel('Probability of winning');
    ylabel('Total reward (continuous lines for Animal A, dashed lines for Animal B)');
    legend(plotsForLegend,stratList);
    
    hold off
    
end
