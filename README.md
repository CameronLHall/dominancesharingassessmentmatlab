dominanceSharingAssessmentMatlab

Matlab code associated with 
"Dominance, sharing, and assessment in an iterated Hawk-Dove game",
In preparation
Authors: Cameron L Hall, Mason A Porter, and Marian S Dawkins

Version 1.0, 22 June 2018

For any bugs, questions or suggestions, contact Cameron Hall at cameron.hall@ul.ie

1. ***Basic Instructions***

1.1 *Most basic instructions*

Look only at the script m-files, especially oneGameScript.m for running single games and tournamentScript.m to run tournaments that compare different strategies. All details can be found by following through the m-files from these scripts. 

1.2 *Running a single game*

Use the script m-file oneGameScript.m

This enables you to specify 
* the cost of winning a fight, cW
* the cost of losing a fight, cL
* the probability of Animal A winning the fight, pA
* the number of rounds, numRounds
* the discount rate (the value of each round relative to previous round), gamma
* the strategies used by Animal A and Animal B
* other details (such as whether to store all the results), as described in the m files.

The strategies for each animal should be chosen from the strategies defined in translateStratName.m and specified using the string for the strategy name.
To add new strategies, see section 1.6.

The main output of oneGameScript is rewardsTotal, a 1-by-2 vector containing the total reward to each animal.
In addition to this, oneGameScript can return resultsStore, a cell array containing detailed results from every round (see m-file for more details).

oneGameScript does not currently include any tools for visualising the results.

1.3 *Running a tournament*

To run tournaments, where multiple strategies are compared, use tournamentScript.m.

tournamentScript.m enables you to specify
* a list of strategies to be considered (pairs of strategies will be constructed and then run against each other).
* the fixed game parameters (cW, cL, number of rounds, discount rate)
* details for constructing the set of pA values to be considered (whether they're drawn from a uniform or beta distribution, whether the complementary probability for each specified probability is used, etc.)

Complete results are given as rewardsStore, which expresses the reward to each animal at the end of each of the games. These are also averaged over repeat runs using the same pA value to give avgRewardsStore.

Results are also given as mainStratComparisonArray, abilityToInvadeArray and variants, which are useful for identifying Evolutionary Stable States (ESSs).
* mainStratComparisonArray is a numStrats-by-numStrats array that gives the average reward (over all runs and all pA values) to each strategy against each other strategy.
  mainStratComparisonArray(i,j) indicates the reward to the ith strategy playing the jth strategy.
* abilityToInvadeArray(i,j) gives mainStratComparisonArray(i,j) - mainStratComparisonArray(j,j). 
  If it is positive, then strategy i can invade strategy j.
  Hence, strategy i will be an ESS amongst the considered strategies if all values in the ith column of abilityToInvadeArray are negative except for the diagonal.
* Variants of abilityToInvadeArray include a rescaling (relative to the maximum possible reward obtainable by an animal) and an assignment of -3 through to 3 to each cell based on how positive or negative each value in the rescaled array is.
  The assignment of -3 to 3 in each cell is visualised automatically. 
  A true ESS should yield a column of blue except for a white diagonal element.

Results may be displayed graphically with a plot for each pair of stratgies, showing how the averaged reward to Animal A and Animal B depend on pA. This is controlled by the flag displayPlotForEachStratPair.
Results may also be displayed graphically with a plot for each strategy showing the reward it receives against all other strategies as a function of the probability that the original strategy wins fights. This is controlled by the flag displayTournamentPlots.
Results for abilityToInvadeArray may also be displayed as a coloured grid, where the colours indicate how positive or negative the values of abilityToInvadeArray are. This is controlled by the flag displayAbilityToInvadeArray.

1.4 *Comparing a pair of strategies (or multiple pairs of strategies)*

To compare pairs of strategies, use compareStratPairsScript.m.

compareStratPairsScript.m enables you to specify
* a list of pairs of strategies to be considered
* the fixed game parameters (cW, cL, number of rounds, discount rate)
* details for constructing the set of pA values to be considered (whether they're drawn from a uniform or beta distribution, whether the complementary pA for each generated pA is used, etc.)

Complete results are given as rewardsStore, which expresses the reward to each animal at the end of each of the games. These are also averaged over repeat runs using the same pA value to give avgRewardsStore.
Results are displayed graphically with a plot for each pair of stratgies, showing how the averaged reward to Animal A and Animal B depend on pA.

1.5 *Running many games (more generally)*

To run many games, it is recommended to use the script m-files compareStratPairsScript.m or tournamentScript.m, detailed in sections 1.3 and 1.4.

Alternatively, the function m-file runManyGames.m can also be used to run multiple games where the game parameters (cW, cL, number of rounds, discount rate) are kept fixed, but different pA values are considered and different strategies pairs are used.
For effective averaging, each chosen pA value can be run multiple times; this is expressed in the input variable numProbRepeats.
The only output stored from runManyGames is rewardsStore, which expresses the reward to each animal at the end of each of the games run by runManyGames.

1.6 *Specifying new strategies*

New strategies can be specified by modifying translateStratName.m as long as the new stratgy is just one that uses the same "model" as existing strategies, but with different parameter values.

Explanations of how to construct new strategies can be found in the comments at the start of translateStratName.m. 
Full details of the parameters of the different strategy types can be found at the end of translateStratName.m.

2 ***Further Details***

To come. Comments are generally fairly good throughout and should provide a useful guide to how the code works.

The main function for running games is runGame.m, which contains a lot of apparent repetition. This is for speed reasons (important when running large tournaments), since it was found that function calling was substantially slowing the processes in Matlab.
