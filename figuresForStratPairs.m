%% figuresForStratPairs.m
% v1.0 22 Jun 2018
%
% Function to generate a figure for each pair of strategies in the list,
% showing the averaged rewards to Animal A and Animal B as a function of
% the probability of Animal A winning a fight. Dashed lines show the
% overall average for each animal.

function ...
    figuresForStratPairs(...
    avgRewardsStore,...         numStratPairs-by-numProbs-by-2 array of the average rewards associated with each strategy pair (dim 1) for each probability of Animal A winning (dim 2) for Animal A and Animal B (dim 3).      
    stratPairsList,...          numStratPairs-by-2 cell array containing the name of each strategy in the pairs considered
    allProbs...                 numProbs-by-1 vector of all pA values used in simulations 
    )

numStratPairs = size(stratPairsList,1);

% Loop through all strategy pairs, creating a figure for each pair that
% compares the rewards to the two strategies involved.
for iStratPairs=1:numStratPairs
    
    figure
    hold on
    
    % Calculate average over all probabilities for Animal A and Animal B as
    % a numStratPairs-by-2 array.
    avgOverProbs = permute(mean(avgRewardsStore,2),[1 3 2]);
    
    % Plot the rewards for Animal A in blue and the rewards for Animal B in
    % red as a function of the probability that Animal A wins a fight.
    % Averages for both Animal A and Animal B are also shown as dashed
    % lines.
    plot(allProbs,avgRewardsStore(iStratPairs,:,1),'Color','b');
    plot(allProbs,avgRewardsStore(iStratPairs,:,2),'Color','r');
    plot([0 1],avgOverProbs(iStratPairs,1)*ones(1,2),'Color','b','LineStyle','--');
    plot([0 1],avgOverProbs(iStratPairs,2)*ones(1,2),'Color','r','LineStyle','--');    
    
    % Title and legend for plot
    title(['Rewards for ''' stratPairsList{iStratPairs,1} ''' against ''' stratPairsList{iStratPairs,2} '''']);
    xlabel('Probability of Animal A winning');
    ylabel('Total reward (red for Animal B)');
    legendText = {stratPairsList{iStratPairs,:}, ...
        [stratPairsList{iStratPairs,1} ' (avg)'], ...
        [stratPairsList{iStratPairs,2} ' (avg)']};
    legend(legendText);
    
    hold off
    
end
