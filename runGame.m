%% runGame.m
% v1.0 22 Jun 2018
%
% Function to run the iterated asymmetric probabilistic Hawk-Dove game
% between two animals. In addition to the game details (as indicated by pA,
% the probability of Animal A winning a fight; and gameParams, a cell array
% containing the costs of fighting, the number of rounds, and the discount
% rate), this requires full details of the strategies being used by both
% animals, as indicated by stratTypes, privParams, and privVars. The main
% output of this function are the total rewards to each animal based on the
% game.
%
% For efficiency reasons (MATLAB has high overheads on repeatedly calling
% functions, which drastically slows the code if there are functions that
% get called every round), this is a long piece of code (by the standards
% of this set of m-files). Major divisions are marked with long strings of
% %%%%%%.

function [...
    rewardsTotal,...                1-by-2 vector containing total rewards for Animal A and Animal B
    resultsStore...                 Store of various results for each animal (only produced if flagged, see note below)
    ] = runGame(...
    pA,...                          Probability of Animal A winnning a fight
    gameParams,...                  Cell array containing parameters for running the game (costs of fighting, number of rounds, discount rate)
    stratTypes,...                  1-by-2 vector of integers indicating type of learning strategy
    privParams,...                  1-by-2 cell array of cell arrays of parameters (specific to learning type)
    privVars,...                    1-by-2 cell array of cell arrays of variables (specific to learning type)
    stratNames...                   1-by-2 cell array of strings indicating strategies (see translateStratName for details)
    )

% resultsStore summary
%  * resultsStore is a 1-by-3 cell array, with the first part corresponding to
%  Animal A, the second to Animal B, and the third to general results.
%  * resultsStore{1} and resultsStore{2} contain strategies in use and
%  (where appropriate) beta parameters.
%  * resultsStore{3} contains the probability of each animal playing Dove,
%  the outcomes for Animal A, and the rewards to each animal from that
%  round.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Initialisations

% Extract parameters from gameParams
% First three parameters indicate fightCosts, number of rounds, and
% discount rate. Subsequent (optional) parameters indicate whether to store
% progressive results, or to include a trembling hand in the results.
fightCosts = gameParams{1};
numRounds = gameParams{2};
discountRate = gameParams{3};
if numel(gameParams) > 3
    storeFlag = gameParams{4};
else
    storeFlag = false;
end
if numel(gameParams) > 4
    trembling = gameParams{5};
else
    trembling = 0;
end

% Vector describing the reward to an animal after a single round as follows
% (move for self listed first):
%                     DD   DH  HD  HH(opp wins)     HH(self wins)
rewardsForOutcomes = [0.5  0   1   -fightCosts(2)   1-fightCosts(1)];

% Vector describing the outcome for Animal B corresponding to a given
% outcome for Animal A (e.g. if Animal A finds that the self and opp moves
% were Dove-Hawk, then Animal B must have experienced Hawk-Dove). A
% experiences        DD DH HD HH(opp wins)  HH(self wins) B experiences
%                    DD HD DH HH(self wins) HH(opp wins)
outcomeForAnimalB = [1  3  2  5             4];

% Initialise the probabilities of each animal playing Dove, whether or not
% they choose Dove in a given round, and the outcomes of each round for
% each player.
doveProbs = zeros(1,2);
doveMoves = false(1,2);
outcomes = zeros(1,2);

% Initialise the memory-one strategy currently in use by each animal, and
% two sets of parameters for the beta distribution (NB. these are not used
% at all by informed strategies, only one set is used by the weighted
% learning strategy, but both sets are used by the threshold-based and
% expected-reward-based learning strategies). In general, this information
% could be stored in betaVars, but storing it in arrays at this level leads
% to greater efficiency.
stratsInUse = zeros(2,5);
betaParamsInUse = zeros(2,4);

% Initialise the amount that results are discounted by.
currDiscount = 1;

% Initialise storage (if appropriate). The first part of resultsStore is
% for Animal A, the second part is for Animal B, and the third part is for
% common results that do not depend on the strategy type (namely the
% probability of choosing Dove, the outcome for Animal A, and the rewards).
if storeFlag
    resultsStore = cell(1,3);
    resultsStore{3} = zeros(numRounds,5);
else
    resultsStore = [];
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% First round

% Loop through both animals, and use methods that depend on the strategy
% type to decide whether to play Dove on the first move, and initialise
% private variables. In the case of a generalised informed memory-one
% strategy (stratType 1), this is also where error checking takes place.
for iAnimal = 1:2
    
    switch stratTypes(iAnimal)
        
        case 0
            %% stratType 0 = Informed memory-one strategy
            
            % Unpack information from privParamSet and privVarSet.
            substratDoveProbs = privParams{iAnimal}{1};
            pWinThresholds = privParams{iAnimal}{2};
            pWin = privVars{iAnimal}{2};
            % Note that privVarSet{1} will indicate the substrategy to be
            % used, which is generated below.
            
            % Determine which substrategy is relevant for the given value
            % of pWin by looping through pWinThresholds until we find a
            % threshold which is above the indicated value of pWin.
            substratChoice = 1;
            for iThresh = 1:numel(pWinThresholds)
                if pWin > pWinThresholds(iThresh)
                    substratChoice = substratChoice + 1;
                elseif pWin == pWinThresholds(iThresh)
                    disp('Probability of winning used in simulation corresponds to a critical threshold where informed strategy changes.')
                    disp(['Problem for strategy ''' stratNames{iAnimal} ''' with pWin = ' num2str(pWin)]);
                    break
                else
                    break
                end
            end
            
            % Use the end of the appropriate row of substratDoveProbs to
            % determine the probability of playing Dove on the first move.
            doveProbs(iAnimal) = substratDoveProbs(substratChoice,6);
            
            % Use this probability to determine the move this turn.
            if doveProbs(iAnimal) == 0
                doveMoves(iAnimal) = false;
            elseif doveProbs(iAnimal) == 1
                doveMoves(iAnimal) = true;
            else
                doveMoves(iAnimal) = (rand<doveProbs(iAnimal));
            end
            
            
            % Use the rest of the appropriate row of substratDoveProbs to
            % establish the memory-one strategy to be used in future turns.
            substratInUse = substratDoveProbs(substratChoice,1:5);
            
            % Memory-one strategy in use goes into stratsInUse. Since this
            % is where the memory-one strategy is now stored (since the
            % code has been updated), the appropriate part of privVars is
            % now cleared.
            stratsInUse(iAnimal,:) = substratInUse;
            privVars{iAnimal}{1} = [];
            
            % If storing results, the only thing to store is the memory-one
            % strategy being used. (The rest of the iAnimal cell of
            % resultsStore will be empty).
            if storeFlag
                resultsStore{iAnimal} = {substratInUse};
            end
            
            
            
        case 1
            %% stratType 1 = Generalised informed memory-one strategy
            
            % Unpack information from privParamSet and privVarSet.
            stratFn = privParams{iAnimal}{1};
            muVector = privVars{iAnimal}{2};
            % Note that privVarSet{1} indicates the substrategy to be used,
            % which will be generated.
            
            % Obtain the 1-by-6 vector containing the memory-one strategy
            % and probability of playing dove on the first move using
            % stratFn.
            stratDoveProbs = stratFn(muVector);
            
            % Check that this strategy is a valid size
            if numel(stratDoveProbs) ~= 6
                error(['Error in definition of ''' stratNames{iAnimal} ...
                    ''' for mu values (' num2str(muVector(1)) ', ' num2str(muVector(2)) ...
                    '): Incorrect size of vector representing strategy']);
            end
            
            % Calculate probability of playing Dove in first round from the
            % strategy vector.
            doveProbs(iAnimal) = stratDoveProbs(6);
            
            % Check that probability is valid, and shift it to 0 or 1 if
            % outside the permissible range.
            if doveProbs(iAnimal) < 0
                doveProbs(iAnimal) = 0;
                disp(['Issue with definition of ''' stratNames{iAnimal} ...
                    ''' for mu values (' num2str(muVector(1)) ', ' num2str(muVector(2)) ...
                    '): Probability of playing Dove in first move is <0']);
            elseif doveProbs(iAnimal) > 1
                doveProbs(iAnimal) = 1;
                disp(['Issue with definition of ''' stratNames{iAnimal} ...
                    ''' for mu values (' num2str(muVector(1)) ', ' num2str(muVector(2)) ...
                    '): Probability of playing Dove in first move is >1']);
            end
            
            % Use this probability to determine the move this turn.
            if doveProbs(iAnimal) == 0
                doveMoves(iAnimal) = false;
            elseif doveProbs(iAnimal) == 1
                doveMoves(iAnimal) = true;
            else
                doveMoves(iAnimal) = (rand<doveProbs(iAnimal));
            end
            
            % Determine memory-one strategy being used in future
            % iterations,
            substratInUse = stratDoveProbs(1:5);
            
            % Check that probabilities are valid, and shift to 0 or 1 if
            % outside the permissible range.
            if any(substratInUse < 0)
                substratInUse(substratInUse<0) = 0;
                disp(['Issue with definition of ''' stratNames{iAnimal} ...
                    ''' for mu values (' num2str(muVector(1)) ', ' num2str(muVector(2)) ...
                    '): Probability of playing Dove in memory-one strategy is <0']);
            end
            if any(substratInUse > 1)
                substratInUse(substratInUse>1) = 1;
                disp(['Issue with definition of ''' stratNames{iAnimal} ...
                    ''' for mu values (' num2str(muVector(1)) ', ' num2str(muVector(2)) ...
                    '): Probability of playing Dove in memory-one strategy is >1']);
            end
            
            % Memory-one strategy in use goes into stratsInUse. Since this
            % is where the memory-one strategy is now stored (since the
            % code has been updated), the appropriate part of privVars is
            % now cleared.
            stratsInUse(iAnimal,:) = substratInUse;
            privVars{iAnimal}{1} = [];

            % If storing results, the only thing to store is the memory-one
            % strategy being used. (The rest of the iAnimal cell of
            % resultsStore will be empty).
            if storeFlag
                resultsStore{iAnimal} = {substratInUse};
            end

        case 10
            %% stratType = 10: Weighted learning strategy
            
            % Unpack information from privParamSet and privVarSet.
            substratDoveProbs = privParams{iAnimal}{1};
            pWinThresholds = privParams{iAnimal}{2};
            betaParams = privVars{iAnimal}{2};
            % Note that privVarSet{1} will indicate the substrategy to be
            % used, which is generated below.
       
            
            % Calculate weightings for substrategies
            if numel(pWinThresholds) >= 1
                substratWeightings = betainc(pWinThresholds',betaParams(1),betaParams(2)); 
                substratWeightings = diff([0 substratWeightings 1]);
            else
                substratWeightings = 1;
            end
            
            % Combine the substrategies to construct a weighted substrategy
            substratInUse = substratWeightings*substratDoveProbs;
            
            % Use the end of the weighted substrategy to
            % determine the probability of playing Dove on the first move.
            doveProbs(iAnimal) = substratInUse(6);
            
            % Once the first move is sorted, the information for the first
            % move can be deleted from privParams (making later code more
            % efficient).
            privParams{iAnimal}{1}(:,6) = [];
            
            % Use this probability to determine the move this turn.
            if doveProbs(iAnimal) == 0
                doveMoves(iAnimal) = false;
            elseif doveProbs(iAnimal) == 1
                doveMoves(iAnimal) = true;
            else
                doveMoves(iAnimal) = (rand<doveProbs(iAnimal));
            end
            
            
            % Use the rest of the appropriate row of substratDoveProbs to
            % establish the memory-one strategy to be used in future turns
            % (until the strategy is updated using results from a fight),
            % and store this in stratsInUse.
            stratsInUse(iAnimal,:) = substratInUse(1:5);
            
            % Store the beta parameters currently in use in
            % betaParamsInUse.
            betaParamsInUse(iAnimal,1:2) = betaParams;

            % Clear the appropriate terms in privVars, since they are not
            % going to be updated in this version of the code.
            privVars{iAnimal}{1} = [];            
            privVars{iAnimal}{2} = [];
            
            % If storing results, the things to store are the memory-one
            % strategy being used and the beta parameters.
            if storeFlag
                resultsStore{iAnimal} = {zeros(numRounds,5) zeros(numRounds,2)};
                resultsStore{iAnimal}{1}(1,:) = substratInUse(1:5);
                resultsStore{iAnimal}{2}(1,:) = betaParams;
            end
            
        case 20
                    
            %% stratType = 20: Estimated pWin learning strategy
            
            % Unpack information from privParamSet and privVarSet.
            substratDoveProbs = privParams{iAnimal}{1};
            pWinThresholds = privParams{iAnimal}{2};
            betaParams = privVars{iAnimal}{4};
            % Note that the other parts of privVars{iAnimal}{1} will be
            % generated below.
       
            % Calculate the estimate of pWin based on initial beta
            % distribution. We use the more optimistic estimate of pWin
            % (which is stored first) to decide the strategy to pursue on
            % the first move.

            pWinEstimate = 1/(1+betaParams(2)/betaParams(1));
            
            % Loop through the thresholds until it is established that the
            % pWin estimate is greater than the given threshold
            numThresholds = numel(pWinThresholds);
            pWinBandChooser = 1;
            for iThresh=1:numThresholds
                if pWinEstimate < pWinThresholds(iThresh)
                    break
                end
                pWinBandChooser = pWinBandChooser + 1;
            end
            
            % Based on the band in use, calculate the threshold expected
            % pWin values for moving up (to a more aggressive strategy) or
            % down (to a less aggressive strategy).
            if pWinBandChooser == 1
                pWinThreshDownUp = [0 pWinThresholds(1)];
            elseif pWinBandChooser == (numThresholds+1)
                pWinThreshDownUp = [pWinThresholds(numThresholds) 1 ];
            else
                pWinThreshDownUp = [pWinThresholds(pWinBandChooser-1) pWinThresholds(pWinBandChooser)];
            end
            
            % Use the seventh term in the appropriate row of
            % subStratDoveProbs to define the probability of playing Dove
            % in the first round.
            doveProbs(iAnimal) = substratDoveProbs(pWinBandChooser,7);
            
            % Use this probability to determine the move this turn.
            if doveProbs(iAnimal) == 0
                doveMoves(iAnimal) = false;
            elseif doveProbs(iAnimal) == 1
                doveMoves(iAnimal) = true;
            else
                doveMoves(iAnimal) = (rand<doveProbs(iAnimal));
            end
            
            % Use the first five terms in the appropriate row of
            % substratDoveProbs to define the memory-one game to be played
            % until pWin goes through one of the thresholds.
            stratsInUse(iAnimal,:) = substratDoveProbs(pWinBandChooser,1:5);
            
            % Store the beta parameters currently in use in
            % betaParamsInUse, and clear this from privVars (since it will
            % not be used there).
            betaParamsInUse(iAnimal,1:4) = betaParams;
            privVars{iAnimal}{4} = [];
            
            % Store the label of the strategy in use and the pWin
            % thresholds for changing strategy in the privVars cell array.
            privVars{iAnimal}{1} = pWinBandChooser;
            privVars{iAnimal}{2} = pWinThreshDownUp;
            
            % If storing results, the things to store are the memory-one
            % strategy being used and the beta parameters.
            if storeFlag
                resultsStore{iAnimal} = {zeros(numRounds,5) zeros(numRounds,4)};
                resultsStore{iAnimal}{1}(1,:) = stratsInUse(iAnimal,:);
                resultsStore{iAnimal}{2}(1,:) = betaParams;
            end
            
        case 30
                    
            %% stratType = 30: Quantile-based learning strategy
            
            % Unpack information from privParamSet and privVarSet.
            substratDoveProbs = privParams{iAnimal}{1};
            pWinThresholds = privParams{iAnimal}{2};
            quantile = privParams{iAnimal}{3};
            betaParams = privVars{iAnimal}{4};
            % Note that the other parts of privVars{iAnimal}{1} will be
            % generated below.
       
            % Calculate the estimate of pWin based on initial beta
            % distribution. We use the more optimistic estimate of pWin
            % (which is stored first) to decide the strategy to pursue on
            % the first move.

            pWinEstimate = betaincinv(quantile,betaParams(1),betaParams(2));
            
            % Loop through the thresholds until it is established that the
            % pWin estimate is greater than the given threshold
            numThresholds = numel(pWinThresholds);
            pWinBandChooser = 1;
            for iThresh=1:numThresholds
                if pWinEstimate < pWinThresholds(iThresh)
                    break
                end
                pWinBandChooser = pWinBandChooser + 1;
            end
            
            % Based on the band in use, calculate the threshold expected
            % pWin values for moving up (to a more aggressive strategy) or
            % down (to a less aggressive strategy).
            if pWinBandChooser == 1
                pWinThreshDownUp = [0 pWinThresholds(1)];
            elseif pWinBandChooser == (numThresholds+1)
                pWinThreshDownUp = [pWinThresholds(numThresholds) 1 ];
            else
                pWinThreshDownUp = [pWinThresholds(pWinBandChooser-1) pWinThresholds(pWinBandChooser)];
            end
            
            % Use the seventh term in the appropriate row of
            % subStratDoveProbs to define the probability of playing Dove
            % in the first round.
            doveProbs(iAnimal) = substratDoveProbs(pWinBandChooser,7);
            
            % Use this probability to determine the move this turn.
            if doveProbs(iAnimal) == 0
                doveMoves(iAnimal) = false;
            elseif doveProbs(iAnimal) == 1
                doveMoves(iAnimal) = true;
            else
                doveMoves(iAnimal) = (rand<doveProbs(iAnimal));
            end
            
            % Use the first five terms in the appropriate row of
            % substratDoveProbs to define the memory-one game to be played
            % until pWin goes through one of the thresholds.
            stratsInUse(iAnimal,:) = substratDoveProbs(pWinBandChooser,1:5);
            
            % Store the beta parameters currently in use in
            % betaParamsInUse, and clear this from privVars (since it will
            % not be used there).
            betaParamsInUse(iAnimal,1:4) = betaParams;
            privVars{iAnimal}{4} = [];
            
            % Store the label of the strategy in use and the pWin
            % thresholds for changing strategy in the privVars cell array.
            privVars{iAnimal}{1} = pWinBandChooser;
            privVars{iAnimal}{2} = pWinThreshDownUp;
            
            % If storing results, the things to store are the memory-one
            % strategy being used and the beta parameters.
            if storeFlag
                resultsStore{iAnimal} = {zeros(numRounds,5) zeros(numRounds,4)};
                resultsStore{iAnimal}{1}(1,:) = stratsInUse(iAnimal,:);
                resultsStore{iAnimal}{2}(1,:) = betaParams;
            end
            
        case 31
                    
            %% stratType = 31: Quantile range learning strategy
            
            % Unpack information from privParamSet and privVarSet.
            substratDoveProbs = privParams{iAnimal}{1};
            pWinThresholds = privParams{iAnimal}{2};
            quantiles = privParams{iAnimal}{3};
            betaParams = privVars{iAnimal}{4};
            % Note that the other parts of privVars{iAnimal}{1} will be
            % generated below.
       
            % Calculate the estimate of pWin based on initial beta
            % distribution. We use the more optimistic estimate of pWin
            % (which is stored first) to decide the strategy to pursue on
            % the first move.

            pWinEstimate = betaincinv(quantiles(1),betaParams(1),betaParams(2));
            
            % Loop through the thresholds until it is established that the
            % pWin estimate is greater than the given threshold
            numThresholds = numel(pWinThresholds);
            pWinBandChooser = 1;
            for iThresh=1:numThresholds
                if pWinEstimate < pWinThresholds(iThresh)
                    break
                end
                pWinBandChooser = pWinBandChooser + 1;
            end
            
            % Based on the band in use, calculate the threshold expected
            % pWin values for moving up (to a more aggressive strategy) or
            % down (to a less aggressive strategy).
            if pWinBandChooser == 1
                pWinThreshDownUp = [0 pWinThresholds(1)];
            elseif pWinBandChooser == (numThresholds+1)
                pWinThreshDownUp = [pWinThresholds(numThresholds) 1 ];
            else
                pWinThreshDownUp = [pWinThresholds(pWinBandChooser-1) pWinThresholds(pWinBandChooser)];
            end
            
            % Use the seventh term in the appropriate row of
            % subStratDoveProbs to define the probability of playing Dove
            % in the first round.
            doveProbs(iAnimal) = substratDoveProbs(pWinBandChooser,7);
            
            % Use this probability to determine the move this turn.
            if doveProbs(iAnimal) == 0
                doveMoves(iAnimal) = false;
            elseif doveProbs(iAnimal) == 1
                doveMoves(iAnimal) = true;
            else
                doveMoves(iAnimal) = (rand<doveProbs(iAnimal));
            end
            
            % Use the first five terms in the appropriate row of
            % substratDoveProbs to define the memory-one game to be played
            % until pWin goes through one of the thresholds.
            stratsInUse(iAnimal,:) = substratDoveProbs(pWinBandChooser,1:5);
            
            % Store the beta parameters currently in use in
            % betaParamsInUse, and clear this from privVars (since it will
            % not be used there).
            betaParamsInUse(iAnimal,1:2) = betaParams;
            privVars{iAnimal}{4} = [];
            
            % Store the label of the strategy in use and the pWin
            % thresholds for changing strategy in the privVars cell array.
            privVars{iAnimal}{1} = pWinBandChooser;
            privVars{iAnimal}{2} = pWinThreshDownUp;
            
            % If storing results, the things to store are the memory-one
            % strategy being used and the beta parameters.
            if storeFlag
                resultsStore{iAnimal} = {zeros(numRounds,5) zeros(numRounds,2)};
                resultsStore{iAnimal}{1}(1,:) = stratsInUse(iAnimal,:);
                resultsStore{iAnimal}{2}(1,:) = betaParams;
            end
            
        otherwise
            
            error(['Error with ''' stratNames{iAnimal} ''': strategy type '...
                num2str(stratTypes(iAnimal)) ' has not been assigned an initialisation method in firstRound.']);
            
    end
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Outcomes of stage game
% Use the moves chosen by each animal and the probability that Animal A
% wins a fight to determine the outcome for each animal (i.e. whether from
% each animal's perspective this round was DD, DH, HD, HH(lose), or
% HH(win)).

% In the case of a trembling hand model, change the outcome for each animal
% with probability trembling.
if trembling ~= 0
    doveProbs = doveProbs + trembling*(1 - 2*doveProbs);
    if (rand<trembling)
        doveMoves(1) = ~doveMoves(1);
    end
    if (rand<trembling)
        doveMoves(2) = ~doveMoves(2);
    end
end


% Use the fact that moveDove = 0 for Hawk and 1 for Dove to determine
% whether the round (from the perspective of Animal A) is DD, DH, HD, HH.
outcomes(1) = 4 - 2*doveMoves(1) - doveMoves(2);

% In the case of Hawk-Hawk, use pA to resolve the outcome of the fight, and
% account for whether it is a win or loss (from the perspective of Animal
% A).
if outcomes(1) == 4
    outcomes(1) = outcomes(1) + (rand<pA);
end

% Use outcomeForAnimalB to determine the outcome for Animal B that
% corresponds to the calculated outcome for Animal A.
outcomes(2) = outcomeForAnimalB(outcomes(1));

% Determine the rewards associated with this first round, and use them to
% initialise the total rewards. For some reason, there seem to be
% significant speed savings from writing the rewardsThisRound update as two
% lines rather than one.
rewardsThisRound(1) = rewardsForOutcomes(outcomes(1));
rewardsThisRound(2) = rewardsForOutcomes(outcomes(2));
rewardsTotal = rewardsThisRound;

% If storing results, use the third cell in resultsStore to store the
% probability of each animal playing Dove, the outcomes for Animal A, and
% the rewards to each animal.
if storeFlag
    resultsStore{3}(1,:) = [doveProbs outcomes(1) rewardsThisRound];
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Subsequent rounds

% Loop through all subsequent rounds
for roundCount = 2:numRounds
    
    % Loop through both animals, calculating the probability of playing
    % dove, the move chosen, and any updates to the private variables.
    for iAnimal = 1:2
        
        switch stratTypes(iAnimal)
            
            case 0        
                %% Simple memory-one strategy
                % stratType = 0 or 1.
                               
                % Use the details of the previous move to determine
                % probability of playing Dove in this move.
                doveProbs(iAnimal) = stratsInUse(iAnimal,outcomes(iAnimal));
                
                % Use this probability to determine the move this turn.
                if doveProbs(iAnimal) == 0
                    doveMoves(iAnimal) = false;
                elseif doveProbs(iAnimal) == 1
                    doveMoves(iAnimal) = true;
                else
                    doveMoves(iAnimal) = (rand<doveProbs(iAnimal));
                end
                
                % NOTE: No additional storage as private variables do not
                % change.
            
            case 1         
                %% Simple memory-one strategy
                % stratType = 0 or 1.
                                
                % Use the details of the previous move to determine
                % probability of playing Dove in this move.
                doveProbs(iAnimal) = stratsInUse(iAnimal,outcomes(iAnimal));
                
                % Use this probability to determine the move this turn.
                if doveProbs(iAnimal) == 0
                    doveMoves(iAnimal) = false;
                elseif doveProbs(iAnimal) == 1
                    doveMoves(iAnimal) = true;
                else
                    doveMoves(iAnimal) = (rand<doveProbs(iAnimal));
                end
                
                % NOTE: No additional storage as private variables do not
                % change.
                
            case 10
                %% Weighted learning strategy
                % stratType = 10.
                
                % Update betaParams if needed.
                if outcomes(iAnimal) == 4
                    
                    % Update beta parameters to include a 'loss'.
                    betaParamsInUse(iAnimal,2) = betaParamsInUse(iAnimal,2)+1;
                    
                elseif outcomes(iAnimal) == 5
 
                    % Update beta parameters to include a 'win'.
                    betaParamsInUse(iAnimal,1) = betaParamsInUse(iAnimal,1)+1;
                    
                end
                
                % If beta parameters have been updated, also update
                % strategy in use.
                
                if outcomes(iAnimal) == 4 || outcomes(iAnimal) == 5 
                    
                    % Unpack information from privParamSet
                    substratDoveProbs = privParams{iAnimal}{1};
                    pWinThresholds = privParams{iAnimal}{2};
                    
                    % Calculate weightings for substrategies.
                    % Recall that privParams{iAnimal}{2} gives the
                    % thresholds for pWin corresponding to the different
                    % substrategies.
                    if numel(pWinThresholds) >= 1
                        substratWeightings = betainc(privParams{iAnimal}{2}',...
                            betaParamsInUse(iAnimal,1),betaParamsInUse(iAnimal,2));
                        substratWeightings = diff([0 substratWeightings 1]);
                    else
                        substratWeightings = 1;
                    end
                    
                    % Combine the substrategies to construct a weighted substrategy
                    stratsInUse(iAnimal,:) = substratWeightings*substratDoveProbs(:,1:5);
                    
                end     
                                
                % Use the details of the previous move to determine
                % probability of playing Dove in this move.
                doveProbs(iAnimal) = stratsInUse(iAnimal,outcomes(iAnimal));
                
                % Use this probability to determine the move this turn.
                if doveProbs(iAnimal) == 0
                    doveMoves(iAnimal) = false;
                elseif doveProbs(iAnimal) == 1
                    doveMoves(iAnimal) = true;
                else
                    doveMoves(iAnimal) = (rand<doveProbs(iAnimal));
                end
                
                % If storing results, the things to store are the
                % memory-one strategy being used and the beta parameters.
                if storeFlag
                    resultsStore{iAnimal}{1}(roundCount,:) = stratsInUse(iAnimal,:);
                    resultsStore{iAnimal}{2}(roundCount,:) = betaParamsInUse(iAnimal,1:2);
                end
                
            case 20
                
                %% stratType = 20: Estimated pWin learning strategy
               
                % Update betaParams if needed.
                if outcomes(iAnimal) == 4
                    
                    % Update beta parameters to include a 'loss'.
                    betaParamsInUse(iAnimal,2) = betaParamsInUse(iAnimal,2)+1;
                    betaParamsInUse(iAnimal,4) = betaParamsInUse(iAnimal,4)+1;
                    
                elseif outcomes(iAnimal) == 5
 
                    % Update beta parameters to include a 'win'.
                    betaParamsInUse(iAnimal,1) = betaParamsInUse(iAnimal,1)+1;
                    betaParamsInUse(iAnimal,3) = betaParamsInUse(iAnimal,3)+1;
                    
                end
                
                % If beta parameters have been updated, check whether
                % estimated pWin values have crossed a threshold. Note that
                % if a fight has been lost, it is only necessary to look at
                % the optimistic pWin estimate; if a fight has been won, it
                % is only necessary to look at the pessimistic pWin
                % estimate.
                
                thresholdCrossed = false;
                if outcomes(iAnimal) == 4 
                    pWinEstimate = 1/(1+betaParamsInUse(iAnimal,2)/betaParamsInUse(iAnimal,1));
                    
                    % Note that privVars{iAnimal}{2} stores the critical
                    % pWin values for shifting down or up in aggression.
                    if pWinEstimate < privVars{iAnimal}{2}(1)
                        thresholdCrossed = true;
                    end
                end
                if outcomes(iAnimal) == 5 
                    pWinEstimate = 1/(1+betaParamsInUse(iAnimal,4)/betaParamsInUse(iAnimal,3));

                    % Note that privVars{iAnimal}{2} stores the critical
                    % pWin values for shifting down or up in aggression.
                    if pWinEstimate > privVars{iAnimal}{2}(2)
                        thresholdCrossed = true;
                    end
                end
                
                if thresholdCrossed

                    %%%%%
                    % If a threshold has been crossed, determine the new
                    % strategy to be pursued.

                    
                    % Unpack information from privParamSet
                    substratDoveProbs = privParams{iAnimal}{1};
                    pWinThresholds = privParams{iAnimal}{2};
                    
                    % Loop through the thresholds until it is established
                    % that the pWin estimate is greater than the given
                    % threshold
                    % N.B. This is needed rather than simply moving up or
                    % down by one band because of the possibility that a
                    % band is skipped.
                    numThresholds = numel(pWinThresholds);
                    pWinBandChooser = 1;
                    for iThresh=1:numThresholds
                        if pWinEstimate < pWinThresholds(iThresh)
                            break
                        end
                        pWinBandChooser = pWinBandChooser + 1;
                    end
                    
                    % Based on the band in use, calculate the new threshold
                    % expected pWin values for moving up (to a more
                    % aggressive strategy) or down (to a less aggressive
                    % strategy).
                    if pWinBandChooser == 1
                        pWinThreshDownUp = [0 pWinThresholds(1)];
                    elseif pWinBandChooser == (numThresholds+1)
                        pWinThreshDownUp = [pWinThresholds(numThresholds) 1];
                    else
                        pWinThreshDownUp = [pWinThresholds(pWinBandChooser-1) pWinThresholds(pWinBandChooser)];
                    end
                    
                    % Use the sixth term in the appropriate row of
                    % subStratDoveProbs to define the probability of
                    % playing Dove in the first round after changing
                    % strategy.
                    doveProbs(iAnimal) = substratDoveProbs(pWinBandChooser,6);
                    
                    % Use the first five terms in the appropriate row of
                    % substratDoveProbs to define the memory-one game to be
                    % played until pWin goes through one of the thresholds.
                    stratsInUse(iAnimal,:) = substratDoveProbs(pWinBandChooser,1:5);      
                                
                    % Set flag (stored in privVars{iAnimal}{3} to indicate
                    % that there has been a change of strategy. This gives
                    % a very simple and temporary "memory-two" feature to
                    % this strategy that is useful for forcing a change to
                    % TfT to start with two Dove moves (and preventing
                    % constant swapping between HD and DH with two TfT
                    % strategies).
                    privVars{iAnimal}{3} = true;
                    
                    % Store the label of the strategy in use and the pWin
                    % thresholds for changing strategy in the privVars cell
                    % array.
                    privVars{iAnimal}{1} = pWinBandChooser;
                    privVars{iAnimal}{2} = pWinThreshDownUp;
                    
                else
                    
                    %%%%%
                    % If no change to strategy in this turn, check whether
                    % there has been a recent (i.e. previous turn) change
                    % to the strategy in use. If so, use the seventh term
                    % in the substrategy definition. Otherwise, just use
                    % the strategy from stratsInUse.
                    
                    if privVars{iAnimal}{3}
                        
                        % Extract dove probabilities for the various
                        % substrategies from privParams and the current
                        % substrategy in use from privVars.
                        substratDoveProbs = privParams{iAnimal}{1};
                        currSubStrat = privVars{iAnimal}{1};
                        
                        % Use the final (seventh) term in the appropriate
                        % substrategy to determine the probabilities of
                        % playing dove in this round.
                        doveProbs(iAnimal) = substratDoveProbs(currSubStrat,7);
                        
                        % Reset flag indicating there has been a recent
                        % change to the strategy.
                        privVars{iAnimal}{3} = false;
                        
                    else
                        
                        % If no recent change to strategy, use the strategy
                        % currently in use and the outcome of the last move
                        % to determine this move.
                        doveProbs(iAnimal) = stratsInUse(iAnimal,outcomes(iAnimal));
                        
                    end
                    
                    
                end
                
                % Use this probability to determine the move this turn.
                if doveProbs(iAnimal) == 0
                    doveMoves(iAnimal) = false;
                elseif doveProbs(iAnimal) == 1
                    doveMoves(iAnimal) = true;
                else
                    doveMoves(iAnimal) = (rand<doveProbs(iAnimal));
                end
                    
                % If storing results, the things to store are the
                % memory-one strategy being used and the beta parameters.
                if storeFlag
                    resultsStore{iAnimal}{1}(roundCount,:) = stratsInUse(iAnimal,:);
                    resultsStore{iAnimal}{2}(roundCount,:) = betaParamsInUse(iAnimal,:);
                end
                
            case 30
                
                %% stratType = 30: Quantile-based learning strategy
               
                % Update betaParams if needed.
                if outcomes(iAnimal) == 4
                    
                    % Update beta parameters to include a 'loss'.
                    betaParamsInUse(iAnimal,2) = betaParamsInUse(iAnimal,2)+1;
                    betaParamsInUse(iAnimal,4) = betaParamsInUse(iAnimal,4)+1;
                    
                elseif outcomes(iAnimal) == 5
 
                    % Update beta parameters to include a 'win'.
                    betaParamsInUse(iAnimal,1) = betaParamsInUse(iAnimal,1)+1;
                    betaParamsInUse(iAnimal,3) = betaParamsInUse(iAnimal,3)+1;
                    
                end
                
                % If beta parameters have been updated, check whether
                % quantile pWin estimates have crossed a threshold. Note that
                % if a fight has been lost, it is only necessary to look at
                % the optimistic pWin estimate; if a fight has been won, it
                % is only necessary to look at the pessimistic pWin
                % estimate.
                
                thresholdCrossed = false;
                if outcomes(iAnimal) == 4 
                    
                    % Note that privParams{iAnimal}{3} stores the quantile
                    % used to decide the pWin estimate.
                    pWinEstimate = betaincinv(privParams{iAnimal}{3},...
                        betaParamsInUse(iAnimal,1),betaParamsInUse(iAnimal,2));
                    
                    % Note that privVars{iAnimal}{2} stores the critical
                    % pWin values for shifting down or up in aggression.
                    if pWinEstimate < privVars{iAnimal}{2}(1)
                        thresholdCrossed = true;
                    end
                end
                if outcomes(iAnimal) == 5 

                    % Note that privParams{iAnimal}{3} stores the quantile
                    % used to decide the pWin estimate.
                    pWinEstimate = betaincinv(privParams{iAnimal}{3},...
                        betaParamsInUse(iAnimal,3),betaParamsInUse(iAnimal,4));

                    % Note that privVars{iAnimal}{2} stores the critical
                    % pWin values for shifting down or up in aggression.
                    if pWinEstimate > privVars{iAnimal}{2}(2)
                        thresholdCrossed = true;
                    end
                end
                
                if thresholdCrossed

                    %%%%%
                    % If a threshold has been crossed, determine the new
                    % strategy to be pursued.

                    
                    % Unpack information from privParamSet
                    substratDoveProbs = privParams{iAnimal}{1};
                    pWinThresholds = privParams{iAnimal}{2};
                    
                    % Loop through the thresholds until it is established
                    % that the pWin estimate is greater than the given
                    % threshold
                    % N.B. This is needed rather than simply moving up or
                    % down by one band because of the possibility that a
                    % band is skipped.
                    numThresholds = numel(pWinThresholds);
                    pWinBandChooser = 1;
                    for iThresh=1:numThresholds
                        if pWinEstimate < pWinThresholds(iThresh)
                            break
                        end
                        pWinBandChooser = pWinBandChooser + 1;
                    end
                    
                    % Based on the band in use, calculate the new threshold
                    % expected pWin values for moving up (to a more
                    % aggressive strategy) or down (to a less aggressive
                    % strategy).
                    if pWinBandChooser == 1
                        pWinThreshDownUp = [0 pWinThresholds(1)];
                    elseif pWinBandChooser == (numThresholds+1)
                        pWinThreshDownUp = [pWinThresholds(numThresholds) 1];
                    else
                        pWinThreshDownUp = [pWinThresholds(pWinBandChooser-1) pWinThresholds(pWinBandChooser)];
                    end
                    
                    % Use the sixth term in the appropriate row of
                    % subStratDoveProbs to define the probability of
                    % playing Dove in the first round after changing
                    % strategy.
                    doveProbs(iAnimal) = substratDoveProbs(pWinBandChooser,6);
                    
                    % Use the first five terms in the appropriate row of
                    % substratDoveProbs to define the memory-one game to be
                    % played until pWin goes through one of the thresholds.
                    stratsInUse(iAnimal,:) = substratDoveProbs(pWinBandChooser,1:5);      
                                
                    % Set flag (stored in privVars{iAnimal}{3} to indicate
                    % that there has been a change of strategy. This gives
                    % a very simple and temporary "memory-two" feature to
                    % this strategy that is useful for forcing a change to
                    % TfT to start with two Dove moves (and preventing
                    % constant swapping between HD and DH with two TfT
                    % strategies).
                    privVars{iAnimal}{3} = true;
                    
                    % Store the label of the strategy in use and the pWin
                    % thresholds for changing strategy in the privVars cell
                    % array.
                    privVars{iAnimal}{1} = pWinBandChooser;
                    privVars{iAnimal}{2} = pWinThreshDownUp;
                    
                else
                    
                    %%%%%
                    % If no change to strategy in this turn, check whether
                    % there has been a recent (i.e. previous turn) change
                    % to the strategy in use. If so, use the seventh term
                    % in the substrategy definition. Otherwise, just use
                    % the strategy from stratsInUse.
                    
                    if privVars{iAnimal}{3}
                        
                        % Extract dove probabilities for the various
                        % substrategies from privParams and the current
                        % substrategy in use from privVars.
                        substratDoveProbs = privParams{iAnimal}{1};
                        currSubStrat = privVars{iAnimal}{1};
                        
                        % Use the final (seventh) term in the appropriate
                        % substrategy to determine the probabilities of
                        % playing dove in this round.
                        doveProbs(iAnimal) = substratDoveProbs(currSubStrat,7);
                        
                        % Reset flag indicating there has been a recent
                        % change to the strategy.
                        privVars{iAnimal}{3} = false;
                        
                    else
                        
                        % If no recent change to strategy, use the strategy
                        % currently in use and the outcome of the last move
                        % to determine this move.
                        doveProbs(iAnimal) = stratsInUse(iAnimal,outcomes(iAnimal));
                        
                    end
                    
                    
                end
                
                % Use this probability to determine the move this turn.
                if doveProbs(iAnimal) == 0
                    doveMoves(iAnimal) = false;
                elseif doveProbs(iAnimal) == 1
                    doveMoves(iAnimal) = true;
                else
                    doveMoves(iAnimal) = (rand<doveProbs(iAnimal));
                end
                    
                % If storing results, the things to store are the
                % memory-one strategy being used and the beta parameters.
                if storeFlag
                    resultsStore{iAnimal}{1}(roundCount,:) = stratsInUse(iAnimal,:);
                    resultsStore{iAnimal}{2}(roundCount,:) = betaParamsInUse(iAnimal,:);
                end
            
            case 31
                %% stratType = 31: Quantile-range learning strategy
               
                % Update betaParams if needed.
                if outcomes(iAnimal) == 4
                    
                    % Update beta parameters to include a 'loss'.
                    betaParamsInUse(iAnimal,2) = betaParamsInUse(iAnimal,2)+1;
                    
                elseif outcomes(iAnimal) == 5
 
                    % Update beta parameters to include a 'win'.
                    betaParamsInUse(iAnimal,1) = betaParamsInUse(iAnimal,1)+1;
                    
                end
                
                % If beta parameters have been updated, check whether
                % quantile pWin estimates have crossed a threshold. Note that
                % if a fight has been lost, it is only necessary to look at
                % the optimistic pWin estimate; if a fight has been won, it
                % is only necessary to look at the pessimistic pWin
                % estimate.
                
                thresholdCrossed = false;
                if outcomes(iAnimal) == 4 
                    
                    % Note that privParams{iAnimal}{3} stores the quantiles
                    % used to decide the pWin estimate.
                    pWinEstimate = betaincinv(privParams{iAnimal}{3}(1),...
                        betaParamsInUse(iAnimal,1),betaParamsInUse(iAnimal,2));
                    
                    % Note that privVars{iAnimal}{2} stores the critical
                    % pWin values for shifting down or up in aggression.
                    if pWinEstimate < privVars{iAnimal}{2}(1)
                        thresholdCrossed = true;
                    end
                end
                if outcomes(iAnimal) == 5 

                    % Note that privParams{iAnimal}{3} stores the quantiles
                    % used to decide the pWin estimate.
                    pWinEstimate = betaincinv(privParams{iAnimal}{3}(2),...
                        betaParamsInUse(iAnimal,1),betaParamsInUse(iAnimal,2));

                    % Note that privVars{iAnimal}{2} stores the critical
                    % pWin values for shifting down or up in aggression.
                    if pWinEstimate > privVars{iAnimal}{2}(2)
                        thresholdCrossed = true;
                    end
                end
                
                if thresholdCrossed

                    %%%%%
                    % If a threshold has been crossed, determine the new
                    % strategy to be pursued.
                    
                    % Unpack information from privParamSet
                    substratDoveProbs = privParams{iAnimal}{1};
                    pWinThresholds = privParams{iAnimal}{2};
                    
                    % Loop through the thresholds until it is established
                    % that the pWin estimate is greater than the given
                    % threshold.
                    % N.B. This is needed rather than simply moving up or
                    % down by one band because of the possibility that a
                    % band is skipped.
                    numThresholds = numel(pWinThresholds);
                    pWinBandChooser = 1;
                    for iThresh=1:numThresholds
                        if pWinEstimate < pWinThresholds(iThresh)
                            break
                        end
                        pWinBandChooser = pWinBandChooser + 1;
                    end
                    
                    % Based on the band in use, calculate the new threshold
                    % expected pWin values for moving up (to a more
                    % aggressive strategy) or down (to a less aggressive
                    % strategy).
                    if pWinBandChooser == 1
                        pWinThreshDownUp = [0 pWinThresholds(1)];
                    elseif pWinBandChooser == (numThresholds+1)
                        pWinThreshDownUp = [pWinThresholds(numThresholds) 1];
                    else
                        pWinThreshDownUp = [pWinThresholds(pWinBandChooser-1) pWinThresholds(pWinBandChooser)];
                    end
                    
                    % Use the sixth term in the appropriate row of
                    % subStratDoveProbs to define the probability of
                    % playing Dove in the first round after changing
                    % strategy.
                    doveProbs(iAnimal) = substratDoveProbs(pWinBandChooser,6);
                    
                    % Use the first five terms in the appropriate row of
                    % substratDoveProbs to define the memory-one game to be
                    % played until pWin goes through one of the thresholds.
                    stratsInUse(iAnimal,:) = substratDoveProbs(pWinBandChooser,1:5);      
                                
                    % Set flag (stored in privVars{iAnimal}{3} to indicate
                    % that there has been a change of strategy. This gives
                    % a very simple and temporary "memory-two" feature to
                    % this strategy that is useful for forcing a change to
                    % TfT to start with two Dove moves (and preventing
                    % constant swapping between HD and DH with two TfT
                    % strategies).
                    privVars{iAnimal}{3} = true;
                    
                    % Store the label of the strategy in use and the pWin
                    % thresholds for changing strategy in the privVars cell
                    % array.
                    privVars{iAnimal}{1} = pWinBandChooser;
                    privVars{iAnimal}{2} = pWinThreshDownUp;
                    
                else
                    
                    %%%%%
                    % If no change to strategy in this turn, check whether
                    % there has been a recent (i.e. previous turn) change
                    % to the strategy in use. If so, use the seventh term
                    % in the substrategy definition. Otherwise, just use
                    % the strategy from stratsInUse.
                    
                    if privVars{iAnimal}{3}
                        
                        % Extract dove probabilities for the various
                        % substrategies from privParams and the current
                        % substrategy in use from privVars.
                        substratDoveProbs = privParams{iAnimal}{1};
                        currSubStrat = privVars{iAnimal}{1};
                        
                        % Use the final (seventh) term in the appropriate
                        % substrategy to determine the probabilities of
                        % playing dove in this round.
                        doveProbs(iAnimal) = substratDoveProbs(currSubStrat,7);
                        
                        % Reset flag indicating there has been a recent
                        % change to the strategy.
                        privVars{iAnimal}{3} = false;
                        
                    else
                        
                        % If no recent change to strategy, use the strategy
                        % currently in use and the outcome of the last move
                        % to determine this move.
                        doveProbs(iAnimal) = stratsInUse(iAnimal,outcomes(iAnimal));
                        
                    end
                    
                    
                end
                
                % Use this probability to determine the move this turn.
                if doveProbs(iAnimal) == 0
                    doveMoves(iAnimal) = false;
                elseif doveProbs(iAnimal) == 1
                    doveMoves(iAnimal) = true;
                else
                    doveMoves(iAnimal) = (rand<doveProbs(iAnimal));
                end
                    
                % If storing results, the things to store are the
                % memory-one strategy being used and the beta parameters.
                if storeFlag
                    resultsStore{iAnimal}{1}(roundCount,:) = stratsInUse(iAnimal,:);
                    resultsStore{iAnimal}{2}(roundCount,:) = betaParamsInUse(iAnimal,1:2);
                end
                
            otherwise
            
            error(['Error with ''' stratNames{iAnimal} ''': strategy type ' num2str(stratTypes(iAnimal)) ' has not been assigned a method in decideMove.']);
            
        end
        
        
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% Outcomes of stage game
    % NB. Mostly identical to previous.
    % Use the moves chosen by each animal and the probability that Animal A
    % wins a fight to determine the outcome for each animal (i.e. whether
    % from each animal's perspective this round was DD, DH, HD, HH(lose),
    % or HH(win)).
    
    % In the case of a trembling hand model, change the outcome for each
    % animal with probability trembling.
    if trembling ~= 0
        % Report a doveProbs value that incorporates the trembling hand.
        % Note that this is OK (i.e. does not keep modifying doveProbs each
        % iteration) since doveProbs is always updated each round.
        doveProbs = doveProbs + trembling*(1 - 2*doveProbs);  
        if (rand<trembling)
        doveMoves(1) = ~doveMoves(1);
        end
        if (rand<trembling)
            doveMoves(2) = ~doveMoves(2);
        end
    end

    % Use the fact that moveDove = 0 for Hawk and 1 for Dove to determine
    % whether the round (from the perspective of Animal A) is DD, DH, HD,
    % HH.
    outcomes(1) = 4 - 2*doveMoves(1) - doveMoves(2);
    
    % In the case of Hawk-Hawk, use pA to resolve the outcome of the fight,
    % and account for whether it is a win or loss (from the perspective of
    % Animal A).
    if outcomes(1) == 4
        outcomes(1) = outcomes(1) + (rand<pA);
    end
    
    % Use outcomeForAnimalB to determine the outcome for Animal B that
    % corresponds to the calculated outcome for Animal A.
    outcomes(2) = outcomeForAnimalB(outcomes(1));
    
    % Update the discount on the rewards to be added this round.
    if discountRate ~= 1
        currDiscount = currDiscount*discountRate;
    end
    
    % Calculate rewards from this round, and update the total rewards
    % (including the effect of the discount rate). For some reason, there
    % seem to be significant speed savings from writing the
    % rewardsThisRound update as two lines rather than one.
    rewardsThisRound(1) = rewardsForOutcomes(outcomes(1));
    rewardsThisRound(2) = rewardsForOutcomes(outcomes(2));
    rewardsTotal = rewardsTotal + rewardsThisRound*currDiscount;

    
    % If storing results, use the third cell in resultsStore to store the
    % probability of each animal playing Dove, the outcomes for Animal A,
    % and the rewards to each animal.
    if storeFlag
        resultsStore{3}(roundCount,:) = [doveProbs outcomes(1) rewardsThisRound];
    end

    
end
