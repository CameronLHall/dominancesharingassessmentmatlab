%% classifyStageGameSeq.m
% v1.0 22 Jun 2018
%
% Function that takes the fightCosts vector (containing the cost of
% fighting to the winner and to the loser), and uses it to classify the
% problem according to the sequence of possible stage games.

function[...
    stageGameSeq,...        Vector of the different stage game classifications.
    pWinThresholds...       Vector of the threshold pA values between different stage games.
    ] = classifyStageGameSeq(...
    fightCosts...           1-by-2 vector of cW (cost of winning a fight) and cL (cost of losing a fight)
    )

%% Compute stage game regime from fightCosts
% Given an allowable fightCosts vector, classify the sequence of stage
% games that will be encountered. To achieve this, test the cost of
% winning, and then the sum of the fightCosts. For a clearer exposition,
% see Figure MM and Table NN in the paper.

if fightCosts(1) < 0.5
    
    if sum(fightCosts) < 0.5
        stageGameSeqRegime = 1;
       
    elseif sum(fightCosts) < 1
        stageGameSeqRegime = 2;
        
    else
        stageGameSeqRegime = 3;
        
    end
    
elseif fightCosts(1) < 1
    
    if sum(fightCosts) < 1
        stageGameSeqRegime = 4;
        
    else
        stageGameSeqRegime = 5;
        
    end
    
else
    stageGameSeqRegime = 6;
        
end

%% Stage game sequence and critical pWin values
% NOTE: The code below should not be modified unless significant changes
% are made to determining the reward to each animal associated with a
% fight.

% Given the regime of the stage game sequence, determine the
% classifications that the stage game passes through, and the pWin values
% corresponding to these classifications. Recall that the different stage
% game classifications are as follows:
% 1 => DL-SD
% 2 => DL-PD
% 3 => PD-SD
% 4 => PD(Self>Opp)
% 5 => PD(Opp>Self)
% 6 => PD-DL
% 7 => SD(Self>Opp)
% 8 => SD(Opp>Self)
% 9 => SD-PD
% 10 => SD-DL
% In this list, the effective game played by an animal itself is listed
% before the effective game played by its opponent. These may be different
% since the game is asymmetric. DL represents Deadlock, PD represents
% Prisoner's Dilemma, and SD represents Snowdrift. In the case where both
% animals are playing the same game, we make a distinction between cases
% where the animal has a greater chance of winning fights than its opponent
% (Self>Opp) and cases where the opponent has a greater chance of winning
% fights (Opp>Self).

% For further details (including expressions for the pWin threshold
% values), see Table NN in the paper.

% Useful variable for computing thresholds
q = 1 - fightCosts(1) + fightCosts(2);

switch stageGameSeqRegime
    
    case 1
        stageGameSeq = [10; 6; 5; 4; 2; 1];
        pWinThresholds = [...
            fightCosts(2)/q;... 
            (0.5 - fightCosts(1))/q;...
            0.5;...
            1 - (0.5 - fightCosts(1))/q;...
            1 - fightCosts(2)/q];
        
    case 2
        stageGameSeq = [10; 9; 5; 4; 3; 1];
        pWinThresholds = [...
            (0.5 - fightCosts(1))/q;...
            fightCosts(2)/q;... 
            0.5;...
            1 - fightCosts(2)/q;...
            1 - (0.5 - fightCosts(1))/q];...
        
    case 3
        stageGameSeq = [10; 9; 8; 7; 3; 1];

        pWinThresholds = [...
            (0.5 - fightCosts(1))/q;...
            (1 - fightCosts(1))/q;... 
            0.5;...
            1 - (1 - fightCosts(1))/q;...
            1 - (0.5 - fightCosts(1))/q];...
     
    case 4   
        stageGameSeq = [9; 5; 4; 3];
        pWinThresholds = [...
            fightCosts(2)/q;... 
            0.5;...
            1 - fightCosts(2)/q];...
        
    case 5
        stageGameSeq = [9; 8; 7; 3];
        pWinThresholds = [...
            (1 - fightCosts(1))/q;... 
            0.5;...
            1 - (1 - fightCosts(1))/q];...

    case 6      
        stageGameSeq = [8; 7];
        pWinThresholds = 0.5;

end
        