%% mixedNashEqmFn.m
% v1.0 22 Jun 2018
%
% Function to define the probability of playing Dove if following a Nash
% equilibrium where the mixed Nash equilibrium is used in the case of a
% Snowdrift stage game.

function probDove = mixedNashEqmFn(muVector)

if muVector(1) > 0
    % Always Hawk if own expected reward from fighting is > 0.
    probDove = 0;
    
elseif muVector(2) > 0
    % Always Dove if own expected reward from fighting is < 0 and
    % opponent's expected reward from fighting is > 0.
    probDove = 1;
    
else
    % Otherwise, use the mixed Nash equilibrium, where the probability of
    % playing Dove is given by a standard game theory result.
    probDove = -2*muVector(1)/(1-2*muVector(1));
    
end
    
