%% tournamentScript.m
% v1.0 22 Jun 2018
%
% Script m-file to run a single tournament that compares various strategies
% for the iterated asymmetric probabilistic Hawk-Dove game and summarises
% these results in numerical form and as figures. 
%
% Inputs are all given in the first section of the code, and include
% * cW and cL (costs to winners and losers associated with fighting)
% * numRounds (number of rounds in game)
% * discountRate (proportional decrease in value of each new round)
% *  stratList (list of strategies to compare in tournament)
% * numProbs (number of different pA values to use in comparing strategies)
% * numProbRepeats (number of repeats to run at each pA value)
%
% Other input parameters can also be modified; they are all described in
% the first section of the code below.
% 
% Main outputs are mainStratComparisonArray and abilityToInvade:
%
% mainStratComparisonArray: 
% A numStrats-by-numStrats matrix indicating the average reward to an
% animal pursuing one strategy against an animal pursuing any other
% strategy from the list provided. This average is obtained over all pA
% values considered.
%
% abilityToInvade: 
% A numStrats-by-numStrats matrix where abilityToInvade(i,j) gives (the
% average reward to strategy i against strategy j) - (the average reward to
% strategy j against itself). abilityToInvade makes it easier to determine
% whether a give strategy is an ESS, since this will correspond to a column
% where all off-diagonal entries are negative. abilityToInvade is also
% shown graphically with red indicating positive values and blue indicating
% negative values.

%% Specify parameters and strategy names

% 1-by-2 vector of cW (cost of winning a fight) and cL (cost of losing a fight)
fightCosts = [0.1 0.2];

% Number of rounds in iterated game
numRounds = 1000;

% Discount rate (if discount rate < 1, future games are worth less than
% past games). Note that 0 < discountRate <= 1.
discountRate = 0.995;

% Trembling hand probability. Probability that each animal changes its
% strategy at the last minute in each round. This should normally be small,
% and definitely be <1/2. While important for certain forms of game
% theoretical stability, the trembling hand makes less sense in a
% biological context and it should normally be set to zero.
trembling = 0;

% List of strategies to be compared in tournament (see index below)
stratList = {...
    'Always Hawk'; ...
    '25% Dove'; ...
    '50% Dove'; ...
    '75% Dove'; ...
    'Always Dove'; ...
    'Tit for Tat'; ...
    'Grim'; ...
    'Pavlov'; ...
    'Modified Pavlov'; ...
    %    'Bully'; ...
    %    'Nash'; ...
    %    'Mixed Nash'; ...
    %    'Selfish'; ...
    %    'Snowdrift TfT'; ...
    %    'PD TfT'; ...
    %    'Informed TfT'; ...
    'Weighted PDTfT with flat prior'; ...
    'Weighted PDTfT with aggressive prior'; ...
    'Weighted PDTfT with very-aggressive prior'; ...
    'Weighted PDTfT with super-aggressive prior'; ...
    'Mean pWin PDTfT with flat prior'; ...
    'Mean pWin PDTfT with aggressive/flat priors'; ...
    'Mean pWin PDTfT with very-aggressive/flat priors'; ...
    'Mean pWin PDTfT with super-aggressive/flat priors'; ...
    'Median pWin PDTfT with flat prior'; ...
    'Median pWin PDTfT with aggressive/flat priors'; ...
    'Median pWin PDTfT with very-aggressive/flat priors'; ...
    'Median pWin PDTfT with super-aggressive/flat priors'; ...
    '70-80 percentile pWin PDTfT with flat prior'; ...
    '80-90 percentile pWin PDTfT with flat prior'; ...
    '90-95 percentile pWin PDTfT with flat prior'; ...
    '95-98 percentile pWin PDTfT with flat prior'; ...
    };

% INDEX OF STRATEGY NAMES (all strategies above should be taken from this
% list)
%
% For full definitions, see translateStratName
%
% NONLEARNING STRATEGIES
% '25% Dove'
% '50% Dove'
% '75% Dove'
% 'Always Dove'
% 'Always Hawk'
% 'Bully'
% 'Grim'
% 'Informed TfT'
% 'Modified Pavlov'
% 'Mixed Nash'
% 'Nash'
% 'Pavlov'
% 'PD TfT'
% 'Selfish'
% 'Snowdrift TfT'
% 'Tit for Tat'
%
% LEARNING STRATEGIES
% 'Weighted PDTfT with flat prior'
% 'Weighted PDTfT with aggressive prior'
% 'Weighted PDTfT with very-aggressive prior'
% 'Weighted PDTfT with super-aggressive prior'
% 'Mean pWin PDTfT with flat prior'
% 'Mean pWin PDTfT with aggressive/flat priors'
% 'Mean pWin PDTfT with very-aggressive/flat priors'
% 'Mean pWin PDTfT with super-aggressive/flat priors'
% 'Median pWin PDTfT with flat prior'
% 'Median pWin PDTfT with aggressive/flat priors'
% 'Median pWin PDTfT with very-aggressive/flat priors'
% 'Median pWin PDTfT with super-aggressive/flat priors'
% '70-80 percentile pWin PDTfT with flat prior'
% '80-90 percentile pWin PDTfT with flat prior'
% '90-95 percentile pWin PDTfT with flat prior'
% '95-98 percentile pWin PDTfT with flat prior'
      
% Flag to indicate whether to run simulations for all possible strategy
% pairs or whether to take advantage of symmetries. For example, should a
% game where Animal A plays Strategy X and Animal B plays Strategy Y be
% 'repeated' where Animal A plays Strategy Y and Animal B plays Strategy X
% or should these be treated as being equivalent.
allStratPairsFlag = true;

% Number of probabilities of Animal A winning to use in tournament.
numProbs = 500;

% Flag to indicate whether the method of complementary random numbers
% should be used. If set to true, allProbs will be constructed so that
% there are numProbs/2 (rounded up) independent probabilities; the other
% probabilities are obtained by taking the complement (pNew = 1 - pOld) of
% these. Using the method of complementary random numbers will reduce the
% variance in the average reward obtained and improve its accuracy.
useComplementaryRandomNumbers = true;

% The probability of Animal A winning a fight is drawn from a symmetric
% beta distribution with this parameter, which must be greater than 0.
% trueBetaParamForWinProbs = 1 corresponds to a uniform distribution of win
% probabilities.
trueBetaParamForWinProbs = 1;

% Number of repeats at each probability
numProbRepeats = 20;

% Flag to indicate whether to display progress after each strategy pair.
% (Particularly useful in large tournaments.)
displayResultsForStratPairs = true;

% Flag to indicate whether to display the "ability to invade" matrix (a
% useful tool for finding possible evolutionary stable states).
displayAbilityToInvadeArray = true;

% Flag to indicate whether to produce a plot for every strategy showing how
% it does against different strategies as the probability of winning
% changes.
displayTournamentPlots = false;

% Flag to indicate whether to produce a plot for every strategy pair. This
% is not recommended for large tournaments
displayPlotForEachStratPair = false;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% PARAMETER SPECIFICATION HAPPENS ABOVE THIS LINE
%%%%% THERE IS NO NEED TO MODIFY CODE BELOW THIS LINE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Collect parameters into cell arrays and check them

% Collect parameters into cell arrays for passing to tournament function

% NOTE: storeFlag is set to false, since the information stored in a one
% game version of this code is not used in the tournament version.

gameParams = {fightCosts, numRounds, discountRate, false, trembling};
stratDetails = {stratList, allStratPairsFlag};
probParams = {numProbs, useComplementaryRandomNumbers, trueBetaParamForWinProbs, numProbRepeats};

% Check that variables in gameParams are within acceptable ranges.
checkGameParams(gameParams);

% Check that stratDetails takes the correct form
if ~iscell(stratDetails{1})
    error('First entry in stratDetails must be a cell array containing the list of strategies for the tournament');
end
if ~islogical(stratDetails{2})
    error('Second entry in stratDetails must be a logical indicating whether or not to consider all possible strategy pairs');
end

% Check that variables in probParams are within acceptable ranges.
checkProbParams(probParams);

%% Construct list of strategy pairs and list of probabilities

% Construct list of strategy pairs for the tournament.
stratPairsList = constructStratPairs(stratDetails);

% Construct list of probabilities to be used in the tournament.
allProbs = constructProbsList(probParams);
numProbs = numel(allProbs);

%% Run tournament

% Use runManyGames to obtain run the main game repatedly using the given
% list of strategies and the given list of probabilities.

% rewardsStore is a numStratPairs-by-numProbs-by-numProbRepeats-by-2 array
% where each entry is the overall reward to a particular animal in a
% particular game.

rewardsStore = runManyGames(gameParams,stratPairsList,allProbs,numProbRepeats,displayResultsForStratPairs);

% avgRewardsStore is a numStratPairs-by-numProbs-by-2 array where each
% entry is the averaged overall reward to a particular animal in a
% particular game (where its opponent and probability of winning a fight
% are kept the same).

avgRewardsStore = permute(mean(rewardsStore,3),[1 2 4 3]);


%% Process and display results from tournament

% NOTE: mainStratComparisonArray is the main output of this work. It
% illustrates the rewards to each strategy against each other strategy.

% If plotting abilityToInvadeArray, calculate a scaling factor to ensure
% fair comparisons between runs with different numbers of rounds or
% different discountRates.
if displayAbilityToInvadeArray
    % We scale all results with respect to the maximum reward achievable
    % (by Always Hawk against Always Dove).
    if discountRate < 1
        scalingAbilityToInvade = (1-discountRate^numRounds)/(1-discountRate);
    else
        scalingAbilityToInvade = numRounds;
    end
    
    % NOTE: Additional scaling (with respect to maximum possible
    % evolutionary stability value) was considered, but this seems to work
    % best.

else
    % Using a negative value for the scalingAbilityToInvade indicates to
    % processTournamentResults that it is not necessary to plot an imagesc
    % colourmap for the final tournament matrix.
    scalingAbilityToInvade = -1; %#ok<UNRCH>
end


% Process results from the tournament to obtain averaged rewards and a
% comparison of the different strategies in a single array
% (mainStratComparisonArray). Additionally generate an array from which it
% is easy to assess evolutionary stability (abilityToInvadeArray), and
% visualise this array if displayAbilityToInvadeArray is set to true.
[mainStratComparisonArray,abilityToInvadeArray,~,abilityToInvadeRescaled,abilityToInvadeForPlot] ...
    = processTournamentResults(avgRewardsStore,stratDetails,scalingAbilityToInvade);

% Plot figures to illustrate how the rewards to a given strategy against
% different opponents change with the probability of winning.
if displayTournamentPlots
    figuresForTournament(avgRewardsStore,stratDetails,allProbs); %#ok<UNRCH>
end

% Construct figures showing the rewards to the different strategies as a
% function of pA. NOTE: This will create a lot of figures if the tournament
% is large.
if displayPlotForEachStratPair
    figuresForStratPairs(avgRewardsStore,stratPairsList,allProbs) %#ok<UNRCH>
end
