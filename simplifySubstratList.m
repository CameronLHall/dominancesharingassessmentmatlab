%% simplifySubstratList.m
% v1.0 22 Jun 2018
%
% Function that takes information about the stage game sequence (and pWin
% thresholds) and combines it with the full private parameter set of the
% strategy to obtain simplified private parameters. Specifically, the first
% two private parameters (detailing
%  * the probabilities of playing Dove in each of the substrategies;
%  * the substrategy associated with each stage game)
% are replaced with new private parameters detailing
%  * the probabilities of playing Dove in each relevant substrategy;
%  * the pWin thresholds at which there are transitions between
%    substrategies.

function[...
    privParamSet...         Simplified cell array of parameters (with first two params changed)
    ] = simplifySubstratList(...
    stageGameSeq,...        Vector of the different stage games encountered as pWin changes  
    pWinThresholds,...      pWin thresholds for transitions between stage game classifications
    privParamSet...         Original cell array of parameters
    )

% Extract substratDoveProbs and substratForStageGameClass from privParamSet
substratDoveProbs = privParamSet{1};
substratForStageGameClass = privParamSet{2};

% Clear first two parameters from privParamSet
privParamSet{1} = [];
privParamSet{2} = [];

% Construct list of relevant substrategies based on stage game sequence
substratsUsed = substratForStageGameClass(stageGameSeq);

% Loop through stage game sequence and determine whether identical
% substrategies are used between one stage game classification and the
% next. If so, these substrategies (and the associted thresholds) can be
% deleted from the list.
transitionsToDelete = false(size(pWinThresholds));
for i = 1:(numel(pWinThresholds)) 
    if substratsUsed(i+1) == substratsUsed(i)
        transitionsToDelete(i) = true;
    end
end
substratsUsed(transitionsToDelete) = [];
pWinThresholds(transitionsToDelete) = [];

% Construct ordered list of the Dove probabilities for the substrategies
% actually being used.
substratDoveProbsUsed = substratDoveProbs(substratsUsed,:);

% Construct simplified private parameters based on the Dove probabilities
% of the substrategies being used and the pWin values associated with
% transitions between these strategies.
privParamSet{1} = substratDoveProbsUsed;
privParamSet{2} = pWinThresholds;



    
    
    
    

