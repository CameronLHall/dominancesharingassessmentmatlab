%% processTournamentResults.m
% v1.0 22 Jun 2018
%
% Function that takes the results from a tournament (in the form of the
% average rewards for each strategy pair) and produces arrays for comparing
% the strategies, visualising these using imagesc if indicated.

function ...
    [mainStratComparisonArray,...   numStrats-by-numStrats array of the average rewards associated with each strategy against each opponent
    abilityToInvadeArray,...        numStrats-by-numStrats array of the difference between the average reward against a given opponent and that opponent's average reward against itself.
    compareStratsArrays,...         numStrats-by-numStrats-by-2 array of the average rewards associated with each strategy against each opponent, separated into Animal A and Animal B results.
    abilityToInvadeRescaled,...     numstrats-by-numstrats array equivalent to abilityToInvadeArray but rescaled with respect to the maximum return from a game.
    abilityToInvadeForPlot...       numstrats-by-numstrats array equivalent to abilityToInvadeArray but reduced to integers indicating how close the rescaled value is to zero.
    ] = processTournamentResults(...
    avgRewardsStore,...             numStratPairs-by-numProbs-by-2 array of the average rewards associated with each strategy pair (dim 1) for each probability of Animal A winning (dim 2) for Animal A and Animal B (dim 3).
    stratDetails,...                1-by-2 cell array containing list of strategies (cell array of strings) and logical indicating whether or not simulations were run for all strategy pairs.
    scalingAbilityToInvade...       Scalar indicating maximum reward to each animal in a game, used for scaling results for plotting. Negative input indicates no plotting.
    )

% Extract information from stratDetails
stratList = stratDetails{1};
allStratPairsFlag = stratDetails{2};

%% Construct arrays for comparing strategies

% Calculate number of strategies
numStrats = numel(stratList);

% Average rewardsStorage over all probabilities of A beating B used, and
% rearrange. This will enable us to compare strategies.
compareStratsStore = permute(mean(avgRewardsStore,2),[1 3 2]);

% Construct numStrats-by-numStrats matrices showing the fully averaged
% rewards to each strategy played against each other strategy.
compareStratsArrays = zeros(numStrats,numStrats,2);

if allStratPairsFlag
    
    % If all strategy pairs are played (including symmetries), the entries
    % in the matrices of fully averaged rewards can easily be read from the
    % arrays of results
    
    % Temporary array for reshaping results from compareStratsStorage
    tempStratsArray = zeros(numStrats);
    
    % Collect and store results for Animal A
    tempStratsArray(:) = compareStratsStore(:,1);
    compareStratsArrays(:,:,1) = tempStratsArray';
    
    % Collect and store results for Animal A
    tempStratsArray(:) = compareStratsStore(:,2);
    compareStratsArrays(:,:,2) = tempStratsArray;
        
    % Arbirarily pick Animal A as the source of the complete array used to
    % compare strategies.
    mainStratComparisonArray = compareStratsArrays(:,:,1);
    
else
    
    % If not all strategy pairs are played, some rearrangement is required
    % in order to construct the upper triangular matrix of rewards for A,
    % and the lower triangular matrix of rewards for B.
    stratPairsCount = 1;
    for iStratA = 1:numStrats
        compareStratsArrays(iStratA,iStratA:end,1)...
            = compareStratsStore(stratPairsCount:stratPairsCount+numStrats-iStratA,1);
        compareStratsArrays(iStratA:end,iStratA,2)...
            = compareStratsStore(stratPairsCount:stratPairsCount+numStrats-iStratA,2);
        stratPairsCount = stratPairsCount+numStrats-iStratA+1;
    end
    
    % Combine the upper triangular matrix of rewards for Animal A and the
    % lower triangular matrix of rewards for Animal B to construct a single
    % array for comparing strategies.
    
    % Remove diagonal from matrix of rewards for Animal B.
    lowerTriangleTemp = compareStratsArrays(:,:,2);
    lowerTriangleTemp(1:numStrats+1:numStrats^2) = 0;
    mainStratComparisonArray = compareStratsArrays(:,:,1) + lowerTriangleTemp;
    
end

% Construct the abilityToInvadeArray which is based on taking the expected
% return to Row Strategy against Column Strategy and subtracting the
% expected return to Column Strategy against itself. If this is negative
% for all offdiagonal terms in a column, then this column represents a
% strategy that cannot be invaded by any of the other strategies
% considered.
selfReward = diag(mainStratComparisonArray);
abilityToInvadeArray =  mainStratComparisonArray - (selfReward*ones(1,size(mainStratComparisonArray,1)))';

% In the case where scalingAbilityToInvade is positive (indicating that
% abilityToInvade should be plotted), use this to rescale the
% abilityToInvadeArray and then plot results based on how far the rescaled
% result is away from zero.
if scalingAbilityToInvade > 0

    abilityToInvadeRescaled = abilityToInvadeArray/scalingAbilityToInvade;
    abilityToInvadeForPlot = ...
        (abilityToInvadeRescaled>0.1) ...
        + (abilityToInvadeRescaled>0.01) ...
        + (abilityToInvadeRescaled>0.001) ...
        - (abilityToInvadeRescaled<-0.001) ...
        - (abilityToInvadeRescaled<-0.01) ...
        - (abilityToInvadeRescaled<-0.1);
    
    figure
    
    % Colourmap chosen to make blues and reds easily distinguishable
    % (including by colourblind people and in BW printouts).
    blueToRed = ...
       [0.1294    0.4000    0.6745;
        0.4039    0.6627    0.8118;
        0.8196    0.8980    0.9412;
        1         1         1;
        0.9922    0.8588    0.7804;
        0.9373    0.5412    0.3843;
        0.6980    0.0941    0.1686];
    colormap(blueToRed);
    imagesc(abilityToInvadeForPlot);
    title('Ability of row strategies to invade column strategies (red positive, blue negative)');
end